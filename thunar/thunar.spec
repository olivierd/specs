%global major 4.17

Name:           thunar
Version:        4.17.8
Release:        99%{?dist}
Summary:        Thunar File Manager

License:        GPLv2+
Source0:        https://archive.xfce.org/src/xfce/thunar/%{major}/%{name}-%{version}.tar.bz2

BuildRequires:	libtool
BuildRequires:  intltool
BuildRequires:  gettext
BuildRequires:	perl

BuildRequires:	pkgconfig(glib-2.0) >= 2.50
BuildRequires:	pkgconfig(gio-2.0) >= 2.50
BuildRequires:	pkgconfig(gmodule-2.0) >= 2.50
BuildRequires:	pkgconfig(gthread-2.0) >= 2.50
BuildRequires:	pkgconfig(gio-unix-2.0) >= 2.50
BuildRequires:	pkgconfig(gtk+-3.0)
BuildRequires:	pkgconfig(gdk-pixbuf-2.0) >= 2.14.0
BuildRequires:  pkgconfig(exo-2) >= 4.17.0
BuildRequires:	pkgconfig(libxfce4util-1.0) >= 4.17.2
BuildRequires:  pkgconfig(libxfce4ui-2) >= 4.17.6
BuildRequires:  pkgconfig(libxfce4kbd-private-3) >= 4.17.6
BuildRequires:	pkgconfig(libxfconf-0)
BuildRequires:	pkgconfig(gobject-introspection-1.0)
BuildRequires:  pkgconfig(gudev-1.0) >= 145
BuildRequires:  pkgconfig(libnotify)
BuildRequires:	pkgconfig(sm)
BuildRequires:	pkgconfig(x11)
BuildRequires:	pkgconfig(ice)
BuildRequires:	pkgconfig(freetype2)
BuildRequires:  pkgconfig(pango)

# For plugins
BuildRequires:  pkgconfig(libpcre)
BuildRequires:  pkgconfig(libexif)
BuildRequires:  pkgconfig(libxfce4panel-2.0) >= 4.16.0

BuildRequires:  desktop-file-utils
BuildRequires:  gtk-update-icon-cache
BuildRequires:	libappstream-glib
Requires:       libxfce4ui >= 4.17.0
Requires:       xfce4-panel >= 4.16.0
Requires:       gobject-introspection
Requires:       shared-mime-info
Requires:       tumbler
Requires:       polkit
Requires:       systemd

# Provide lowercase name to help people find the package. 
Provides:       thunar = %{version}-%{release}

%description
Thunar is a new modern file manager for the Xfce Desktop Environment. It has 
been designed from the ground up to be fast and easy-to-use. Its user interface 
is clean and intuitive, and does not include any confusing or useless options. 
Thunar is fast and responsive with a good start up time and directory load time.

%package devel
Summary:	Development tools for Thunar file manager
Requires:	%{name}%{?_isa} = %{version}-%{release}
Requires:	glib2-devel
Requires:	gtk3-devel
Requires:	gobject-introspection-devel

%description devel
libraries and header files for the Thunar file manager.

%prep
%setup -q

# fix icon in thunar-sendto-email.desktop
%{__sed} -i 's!mail-send!org.xfce.mailreader!' \
	plugins/thunar-sendto-email/thunar-sendto-email.desktop.in.in

%build
%configure --disable-static \
	--enable-introspection=yes \
	--enable-gio-unix --enable-gudev \
	--enable-exif --enable-pcre \
	--without-html-dir

# Remove rpaths
%{__sed} -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
%{__sed} -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

# Needed for gobject-introspection build, because we killed RPATH
export LD_LIBRARY_PATH=$(pwd)/thunarx/.libs:${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}

%make_build

%install
%make_install

find %{buildroot} -name '*.la' | xargs %{__rm}

%find_lang thunar

%check
appstream-util validate-relax --nonet \
	%{buildroot}%{_datadir}/metainfo/org.xfce.thunar.appdata.xml

%ldconfig_scriptlets devel

%post
%systemd_user_post thunar.service
update-desktop-database &> /dev/null ||:
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%preun
%systemd_user_preun thunar.service

%postun
%systemd_user_postun thunar.service
update-desktop-database &> /dev/null ||:
if [ $1 -eq 0 ] ; then
	touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files -f thunar.lang
%defattr(-,root,root,-)
%license COPYING COPYING.LIB
%doc AUTHORS FAQ INSTALL NEWS README.md THANKS
%{_bindir}/Thunar
%{_bindir}/thunar
%{_bindir}/thunar-settings
%{_libdir}/libthunarx-3.so.*
%{_libdir}/thunarx-3/thunar-apr.so
%{_libdir}/thunarx-3/thunar-sbr.so
%{_libdir}/thunarx-3/thunar-uca.so
%{_libdir}/thunarx-3/thunar-wallpaper-plugin.so
%{_libdir}/Thunar/thunar-sendto-email
%{_libdir}/xfce4/panel/plugins/libthunar-tpa.so
%{_datadir}/Thunar/sendto/*.desktop
%{_datadir}/polkit-1/actions/org.xfce.thunar.policy
%{_datadir}/applications/*.desktop
%{_datadir}/dbus-1/services/org.xfce.FileManager.service
%{_datadir}/dbus-1/services/org.xfce.Thunar.service
%{_datadir}/dbus-1/services/org.xfce.Thunar.FileManager1.service
%{_datadir}/icons/hicolor/*/*/*
%{_datadir}/xfce4/panel/plugins/thunar-tpa.desktop
%{_metainfodir}/org.xfce.thunar.appdata.xml
%{_mandir}/man1/Thunar.1.gz
%{_userunitdir}/thunar.service
%exclude %{_docdir}/thunar/README.gtkrc
%config(noreplace) %{_sysconfdir}/xdg/Thunar/uca.xml

%files devel
%doc HACKING
%{_includedir}/thunarx-3/thunarx/*
%{_libdir}/libthunarx-3.so
%{_libdir}/girepository-1.0/Thunarx-3.0.typelib
%{_libdir}/pkgconfig/thunarx-3.pc
%{_datadir}/gir-1.0/Thunarx-3.0.gir

%changelog
* Sun Apr 03 2022 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.17.8

* Mon Nov 29 2021 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.17.7

* Mon Sep 27 2021 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.17.6

* Sun Sep 12 2021 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.17.5

* Sun Jul 18 2021 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.17.4

* Sun May 09 2021 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.17.3

* Sun Mar 21 2021 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.17.1

* Mon Jan 25 2021 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.17.0

* Wed Jan 13 2021 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.16.2

* Wed Dec 30 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.16.1

* Sat Dec 26 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.16.0

* Sun Nov 01 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.15.3

* Mon Aug 24 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.15.2

* Thu Jul 02 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.15.1

* Sun Jun 21 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.15.0

* Sun May 24 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.8.15

* Wed Mar 25 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.8.14

* Tue Mar 24 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.8.13

* Fri Jan 31 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.8.12

* Sat Nov 16 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.8.11

* Sun Nov 10 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.8.10

* Mon Aug 12 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.8.9

* Sat Jul 20 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.8.8

* Sat Jun 29 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.8.7

* Sat Jun 22 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Rebuild for Fedora 30

* Sun May 19 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.8.6

* Sun Jan 27 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.8.4

* Fri Jan 25 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.8.3

* Wed Nov 07 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Sync with Fedora 29

* Wed Sep 26 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.8.2

* Tue Jun 19 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Apply upstream patch (Avoid use of invalid pointer values in UCA)

* Thu Jun 14 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.8.1

* Thu Jun 07 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.8.0

* Sat Apr 07 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.7.2

* Fri Feb 16 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.7.1

* Sat Nov 25 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.6.13

* Mon Jul 17 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Rebuilt after update of tumbler (ported to GDbus)

* Sat Jul 15 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Rebuilt for update of tumbler

* Sun Jul 02 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.6.12

* Mon Feb 13 2017 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 1.6.11-1
- Update to 1.6.11
- Dropped upstreamed patches

* Fri Feb 10 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.6.10-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Thu May 19 2016 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 1.6.10-6
- Fix typo in patching

* Thu May 19 2016 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 1.6.10-5
- Mitigate move and rename crashes - patches added from upstream

* Wed Feb 03 2016 Fedora Release Engineering <releng@fedoraproject.org> - 1.6.10-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Fri Sep 18 2015 Richard Hughes <rhughes@redhat.com> - 1.6.10-3
- Remove no longer required AppData file

* Tue Jun 16 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.6.10-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Fri May 22 2015 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 1.6.10-1
- Update to 1.6.10 - bugfix update

* Sun May 17 2015 Kevin Fenzi <kevin@scrye.com> 1.6.9-1
- Update to 1.6.9. Drop gtk-docs they are no longer supported upstream.

* Tue May 05 2015 Kevin Fenzi <kevin@scrye.com> 1.6.8-1
- Update to 1.6.8.

* Mon Apr 20 2015 Kevin Fenzi <kevin@scrye.com> 1.6.7-1
- Update to 1.6.7. Fixes bug #1183644

* Thu Mar 26 2015 Richard Hughes <rhughes@redhat.com> - 1.6.6-2
- Add an AppData file for the software center

* Sat Feb 28 2015 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 1.6.6-1
- Update to 1.6.6

* Sat Feb 21 2015 Till Maas <opensource@till.name> - 1.6.5-2
- Rebuilt for Fedora 23 Change
  https://fedoraproject.org/wiki/Changes/Harden_all_packages_with_position-independent_code

* Thu Feb 19 2015 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 1.6.5-1
- Update to 1.6.5
- Moved COPYING file to license directory
- Thunar-tpa desktop file moved to ..xfce4/panel/plugins/..

* Sun Jan 04 2015 Kevin Fenzi <kevin@scrye.com> 1.6.4-1
- Update to 1.6.4

* Sun Dec 21 2014 Kevin Fenzi <kevin@scrye.com> 1.6.3-6
- Add patch for glib2 open with ordering. Fixes bug #1175867

* Tue Nov 11 2014 Kevin Fenzi <kevin@scrye.com> 1.6.3-5
- Add appdata. Fixes bug #1161931

* Fri Aug 15 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.6.3-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Fri Jun 06 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.6.3-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Fri Aug 02 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.6.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Sun May 05 2013 Kevin Fenzi <kevin@scrye.com> 1.6.3-1
- Update to 1.6.3

* Fri Feb 22 2013 Toshio Kuratomi <toshio@fedoraproject.org> - 1.6.2-3
- Remove --vendor from desktop-file-install https://fedorahosted.org/fesco/ticket/1077

* Wed Feb 13 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.6.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Thu Dec 27 2012 Kevin Fenzi <kevin@scrye.com> 1.6.2-1
- Update to 1.6.2
- Clean up changelog

* Sun Dec 09 2012 Kevin Fenzi <kevin@scrye.com> 1.6.1-1
- Update to 1.6.1

* Tue Dec 04 2012 Kevin Fenzi <kevin@scrye.com> 1.6.0-1
- Update to 1.6.0. 
- See http://git.xfce.org/xfce/thunar/tree/NEWS?id=781395f339e13f4da7c69ac63caefeec451b6dea

* Sat Oct 13 2012 Christoph Wickert <cwickert@fedoraproject.org> - 1.4.0-3
- Show 'Send to' menu entries based on filetypes
- Add blueman-sendto to the 'Sent to' menu

* Wed Jul 18 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.4.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Sun Apr 29 2012 Christoph Wickert <cwickert@fedoraproject.org> - 4.10.0-1
- Update to 1.4.0 (Xfce 4.10 final)
- Make build verbose
- Add VCS key

* Sat Apr 14 2012 Kevin Fenzi <kevin@scrye.com> - 1.3.2-1
- Update to 1.3.2 (Xfce 4.10pre2)

* Mon Apr 02 2012 Kevin Fenzi <kevin@scrye.com> - 1.3.1-1
- Update to 1.3.1

* Fri Feb 10 2012 Petr Pisar <ppisar@redhat.com> - 1.3.0-7
- Rebuild against PCRE 8.30

* Thu Jan 12 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.3.0-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Mon Oct 10 2011 Christoph Wickert <cwickert@fedoraproject.org> - 1.3.0-5
- Own %%{_libdir}/Thunar/

* Thu Apr 21 2011 Christoph Wickert <cwickert@fedoraproject.org> - 1.3.0-4
- Fix format string flaw CVE-2011-1588 (#698290)

* Tue Mar 15 2011 Christoph Wickert <cwickert@fedoraproject.org> - 1.3.0-3
- Add missing BRs: libexif-devel, libICE-devel and libnotify-devel

* Tue Mar 08 2011 Christoph Wickert <cwickert@fedoraproject.org> - 1.3.0-2
- Obsolete old plugins (#682491)
- Add sendto helper for quodlibet

* Mon Feb 14 2011 Christoph Wickert <cwickert@fedoraproject.org> - 1.3.0-1
- Update to 1.3.0

* Mon Feb 07 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.2.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sun Jan 30 2011 Kevin Fenzi <kevin@tummy.com> - 1.2.1-1
- Update to 1.2.1

* Sat Jan 22 2011 Kevin Fenzi <kevin@tummy.com> - 1.2.0-2
- Add hack for upgrades (works around bug #670210)

* Sun Jan 16 2011 Kevin Fenzi <kevin@tummy.com> - 1.2.0-1
- Update to 1.2.0

* Sun Jan 02 2011 Christoph Wickert <cwickert@fedoraproject.org> - 1.1.6-1
- Update to 1.1.6

* Sun Dec 05 2010 Christoph Wickert <cwickert@fedoraproject.org> - 1.1.5-1
- Update to 1.1.5

* Mon Nov 08 2010 Christoph Wickert <cwickert@fedoraproject.org> - 1.1.4-1
- Update to 1.1.4
- Drop obsolete build requirements: GConf2-devel, fam-devel, hal-devel, 
  libjpeg-devel, libxslt-devel.
- Remove old patches

* Mon Nov 01 2010 Kevin Fenzi <kevin@tummy.com> - 1.0.2-4
- Add patch for trash icon. (#647734)

* Sat Oct 16 2010 Kevin Fenzi <kevin@tummy.com> - 1.0.2-3
- Add patch for Drag and drop issue. (#633171)

* Thu Jun 17 2010 Christoph Wickert <cwickert@fedoraproject.org> - 1.0.2-2
- Fix conditional requirement for hal-storage-addon

* Fri May 21 2010 Kevin Fenzi <kevin@tummy.com> - 1.0.2-1
- Update to 1.0.2

* Fri Apr 30 2010 Christoph Wickert <cwickert@fedoraproject.org> - 1.0.1-7
- Require hal-storage-addon
- Remove obsolete mime types (#587256)
- Update icon-cache scriptlets

* Thu Apr 15 2010 Kevin Fenzi <kevin@tummy.com> - 1.0.1-6
- Bump release

* Thu Apr 15 2010 Kevin Fenzi <kevin@tummy.com> - 1.0.1-5
- Add patch to fix directory umask issue. Fixes bug #579087

* Sat Feb 13 2010 Kevin Fenzi <kevin@tummy.com> - 1.0.1-4
- Add patch for DSO linking. Fixes bug #564714

* Thu Sep 10 2009 Kevin Fenzi <kevin@tummy.com> - 1.0.1-3
- Require dbus-x11 (#505499)

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Sun Apr 19 2009 Kevin Fenzi <kevin@tummy.com> - 1.0.1-1
- Update to 1.0.1

* Thu Feb 26 2009 Kevin Fenzi <kevin@tummy.com> - 1.0.0-1
- Update to 1.0.0

* Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.99.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Jan 26 2009 Kevin Fenzi <kevin@tummy.com> - 0.9.99.1-1
- Update to 0.9.99.1

* Tue Jan 13 2009 Kevin Fenzi <kevin@tummy.com> - 0.9.93-1
- Update to 0.9.93

* Fri Dec 26 2008 Kevin Fenzi <kevin@tummy.com> - 0.9.92-1
- Update to 0.9.92

* Mon Oct 27 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.9.3-1
- Update to 0.9.3
- Respect xdg user directory paths (#457740)
- Don't spawn zombies (bugzilla.xfce.org #2983)
- Add additional sendto helpers for bluethooth and audaciuos (#450784)

* Sat Feb 23 2008 Kevin Fenzi <kevin@tummy.com> - 0.9.0-4
- Remove requires on xfce-icon-theme. See bug 433152

* Sun Feb 10 2008 Kevin Fenzi <kevin@tummy.com> - 0.9.0-3
- Rebuild for gcc43

* Mon Dec  3 2007 Kevin Fenzi <kevin@tummy.com> - 0.9.0-2
- Add thunar-vfs patch. 

* Sun Dec  2 2007 Kevin Fenzi <kevin@tummy.com> - 0.9.0-1
- Update to 0.9.0

* Mon Aug 27 2007 Kevin Fenzi <kevin@tummy.com> - 0.8.0-3
- Update License tag

* Mon Jul  9 2007 Kevin Fenzi <kevin@tummy.com> - 0.8.0-2
- Add provides for lowercase name

* Sun Jan 21 2007 Kevin Fenzi <kevin@tummy.com> - 0.8.0-1
- Upgrade to 0.8.0

* Mon Dec 18 2006 Kevin Fenzi <kevin@tummy.com> - 0.5.0-0.3.rc2
- Own the thunarx-1 directory

* Sat Nov 11 2006 Kevin Fenzi <kevin@tummy.com> - 0.5.0-0.2.rc2
- Increase exo version 

* Thu Nov 09 2006 Kevin Fenzi <kevin@tummy.com> - 0.5.0-0.1.rc2
- Upgrade to 0.5.0rc2

* Mon Oct 09 2006 Kevin Fenzi <kevin@tummy.com> - 0.4.0-0.11.rc1
- Add shared-mime-info and xfce4-icon-theme as Requires (fixes #209592)

* Fri Oct 06 2006 Kevin Fenzi <kevin@tummy.com> - 0.4.0-0.10.rc1
- Tweak Obsoletes versions

* Fri Oct 06 2006 Kevin Fenzi <kevin@tummy.com> - 0.4.0-0.9.rc1
- Obsolete xffm for now. 

* Thu Oct 05 2006 Kevin Fenzi <kevin@tummy.com> - 0.4.0-0.8.rc1
- Really re-enable the trash plugin. 

* Thu Oct 05 2006 Kevin Fenzi <kevin@tummy.com> - 0.4.0-0.7.rc1
- Re-enable trash plugin in Xfce 4.4rc1

* Thu Oct 05 2006 Christian Iseli <Christian.Iseli@licr.org> - 0.4.0-0.6.rc1
 - rebuilt for unwind info generation, broken in gcc-4.1.1-21

* Sat Sep 16 2006 Kevin Fenzi <kevin@tummy.com> - 0.4.0-0.5.rc1
- Remove duplicate thunar-sendto-email.desktop entry from files. 

* Fri Sep 15 2006 Kevin Fenzi <kevin@tummy.com> - 0.4.0-0.4.rc1
- Added Requires: exo-devel >= 0.3.1.10 to devel. 
- exclude docs moved from datadir to docs
- Fixed datdir including files twice

* Thu Sep 14 2006 Kevin Fenzi <kevin@tummy.com> - 0.4.0-0.3.rc1
- Cleaned up BuildRequires some more
- Disabled tpa plugin and desktop for now
- Moved some files from doc/Thunar to be %%doc
- Changed man to use wildcard in files
- Added examples to devel subpackage
- Made sure some examples are not executable. 

* Tue Sep 12 2006 Kevin Fenzi <kevin@tummy.com> - 0.4.0-0.2.rc1
- Added some BuildRequires
- Added --with-gtkdoc and gtkdoc files to devel

* Wed Sep  6 2006 Kevin Fenzi <kevin@tummy.com> - 0.4.0-0.1.rc1
- Inital package for fedora extras

