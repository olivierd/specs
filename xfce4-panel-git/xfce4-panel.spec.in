%global date ##DATE##
%global commit ##HASH##
%global shortcommit %(c=%{commit}; echo ${c:0:7})

Name:           xfce4-panel
Version:        %{date}
Release:        3.git%{shortcommit}%{?dist}
Summary:        Next generation panel for Xfce

Group:          User Interface/Desktops
License:        GPLv2+ and LGPLv2+
URL:            http://www.xfce.org/
#VCS git:git://git.xfce.org/xfce/xfce4-panel
Source0:        https://git.xfce.org/xfce/%{name}/snapshot/%{name}-%{commit}.tar.bz2

# Needed when we launch autogen.sh script
BuildRequires:	autoconf
BuildRequires:	automake
BuildRequires:	xfce4-dev-tools
BuildRequires:	gtk-doc

BuildRequires:	intltool
BuildRequires:	gettext
BuildRequires:	perl
BuildRequires:	libtool

BuildRequires:	pkgconfig(libxfce4util-1.0)
BuildRequires:	pkgconfig(garcon-1) >= 0.6.0
BuildRequires:	pkgconfig(garcon-gtk3-1) >= 0.6.0
BuildRequires:	pkgconfig(libxfce4ui-2) >= 4.13.0
BuildRequires:	pkgconfig(libxfconf-0) >= 4.13.2
BuildRequires:	pkgconfig(exo-2) >= 0.11.2
BuildRequires:	pkgconfig(gtk+-3.0)
BuildRequires:	pkgconfig(gtk+-2.0)
BuildRequires:	pkgconfig(glib-2.0)
BuildRequires:	pkgconfig(gio-2.0)
BuildRequires:	pkgconfig(gio-unix-2.0)
BuildRequires:	pkgconfig(gmodule-2.0)
BuildRequires:	pkgconfig(cairo)
BuildRequires:	pkgconfig(libwnck-3.0)

BuildRequires:	libX11-devel
BuildRequires:	desktop-file-utils

%description
This package includes the panel for the Xfce desktop environment.

%package gtk2
Summary:	Gtk+ 2.x support for %{name}
Group:	User Interface/Desktops
License:	GPLv2+ and LGPLv2+
Requires:	%{name}%{?_isa} = %{version}-%{release}

%description gtk2
Gtk+ 2.x support for %{name}.

%package devel
Summary:        Development headers for %{name}
Group:          Development/Libraries
Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       %{name}-gtk2%{?_isa} = %{version}-%{release}
Requires:       pkgconfig
Requires:       gtk2-devel%{?_isa}
Requires:       gtk3-devel%{?_isa}
Requires:       glib2-devel%{?_isa}
Requires:       libxfce4util-devel%{?_isa} >= 4.13.0

%description devel
This package includes libraries and header files for %{name}.


%prep
%setup -q -n %{name}-%{commit}

# Fix icon in 'Add new panel item' dialog
%{__sed} -i 's|Icon=office-calendar|Icon=x-office-calendar|' plugins/clock/clock.desktop.in.in


%build
(if ! test -x configure; then ./autogen.sh; fi;
	%configure --disable-static --enable-gtk2 --enable-gio-unix --enable-maintainer-mode --without-html-dir)

# Remove rpaths
%{__sed} -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
%{__sed} -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

make %{?_smp_mflags} V=1


%install
make install DESTDIR=%{buildroot} INSTALL="%{__install} -p"

# fix permissions for installed libs
%{__chmod} 755 $RPM_BUILD_ROOT/%{_libdir}/*.so

find %{buildroot} -name '*.la' | xargs %{__rm}

%find_lang %{name}

%check
desktop-file-validate $RPM_BUILD_ROOT/%{_datadir}/applications/panel-desktop-handler.desktop
desktop-file-validate $RPM_BUILD_ROOT/%{_datadir}/applications/panel-preferences.desktop

%post
/sbin/ldconfig
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :


%postun
/sbin/ldconfig
if [ $1 -eq 0 ] ; then
  touch --no-create %{_datadir}/icons/hicolor &>/dev/null
  gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi


%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :


%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS
%license COPYING COPYING.LIB
%config(noreplace) %{_sysconfdir}/xdg/xfce4/panel/default.xml
%{_bindir}/*
%{_libdir}/libxfce4panel-2.0.so.*
%{_libdir}/xfce4/panel/migrate
%{_libdir}/xfce4/panel/plugins/*
%{_libdir}/xfce4/panel/wrapper-2.0
%{_datadir}/icons/hicolor/*/*/*
%{_datadir}/xfce4/panel/*/*
%{_datadir}/applications/*.desktop

%files gtk2
%{_libdir}/libxfce4panel-1.0.so.*
%{_libdir}/xfce4/panel/wrapper-1.0

%files devel
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/*
%{_libdir}/libxfce4panel-*.so
%{_includedir}/xfce4/libxfce4panel-*/*

%changelog
* Fri Jul 14 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to the latest snapshot

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 4.12.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Tue Oct 25 2016 Kevin Fenzi <kevin@scrye.com> - 4.12.1-1
- Update to 4.12.1. Fixes bug #1388439

* Fri Feb 05 2016 Fedora Release Engineering <releng@fedoraproject.org> - 4.12.0-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Fri Jun 19 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.12.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Tue Mar 10 2015 Kevin Fenzi <kevin@scrye.com> 4.12.0-3
- Obsolete some panel plugins we are dropping with Fedora 22.

* Sat Feb 28 2015 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 4.12.0-2
- Build with GTK3 support

* Sat Feb 28 2015 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 4.12.0-1
- Update to stable release 4.12.0

* Sat Feb 21 2015 Till Maas <opensource@till.name> - 4.10.1-7
- Rebuilt for Fedora 23 Change
  https://fedoraproject.org/wiki/Changes/Harden_all_packages_with_position-independent_code

* Mon Aug 18 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.10.1-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Sun Jun 08 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.10.1-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Mon Dec 02 2013 Kevin Fenzi <kevin@scrye.com> 4.10.1-4
- Rebuild for new libwnck

* Mon Oct 21 2013 Kevin Fenzi <kevin@scrye.com> 4.10.1-3
- Add patch to fix autohide and session menu issue. Fixes bug #1021548

* Sun Aug 04 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.10.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Sun May 05 2013 Kevin Fenzi <kevin@scrye.com> 4.10.1-1
- Update to 4.10.1

* Fri Jul 27 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.10.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Sun Apr 29 2012 Christoph Wickert <cwickert@fedoraproject.org> - 4.10.0-1
- Update to 4.10.0 final
- Make build verbose
- Add VCS key

* Sat Apr 14 2012 Kevin Fenzi <kevin@scrye.com> - 4.9.2-1
- Update to 4.9.2 (Xfce 4.10pre2)

* Mon Apr 02 2012 Kevin Fenzi <kevin@scrye.com> - 4.9.1-1
- Update to 4.9.1

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.8.6-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Wed Oct 26 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.8.6-3
- Rebuilt for glibc bug#747377

* Sun Oct 23 2011 Christoph Wickert <cwickert@fedoraproject.org> - 4.8.6-2
- Fix directory menu plugin's 'Open in Terminal' option (#748226)
- No longer depend on xfce4-doc (#721288)

* Thu Sep 22 2011 Christoph Wickert <cwickert@fedoraproject.org> - 4.8.6-1
- Update to 4.8.6

* Tue Jun 21 2011 Christoph Wickert <cwickert@fedoraproject.org> - 4.8.5-1
- Update to 4.8.5

* Sun Jun 19 2011 Christoph Wickert <cwickert@fedoraproject.org> - 4.8.4-1
- Update to 4.8.4

* Sun May 08 2011 Christoph Wickert <wickert@kolabsys.com> - 4.8.3-2
- Add xfce4-clock icons for the 'Add new items' dialog (#694902)

* Wed Apr 06 2011 Christoph Wickert <cwickert@fedoraproject.org> - 4.8.3-1
- Update to 4.8.3
- Remove upstreamed add_button_release_event_to_proxy_item.patch

* Fri Mar 25 2011 Christoph Wickert <cwickert@fedoraproject.org> - 4.8.2-3
- Fix switching grouped windows in the taskbar (#680779)

* Tue Mar 08 2011 Christoph Wickert <cwickert@fedoraproject.org> - 4.8.2-2
- Include mixer in default panel config (#636227)
- Obsolete old plugins (#682491)

* Fri Feb 25 2011 Christoph Wickert <cwickert@fedoraproject.org> - 4.8.2-1
- Update to 4.8.2

* Mon Feb 07 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.8.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sun Jan 30 2011 Kevin Fenzi <kevin@tummy.com> - 4.8.1-1
- Update to 4.8.1

* Sun Jan 16 2011 Kevin Fenzi <kevin@tummy.com> - 4.8.0-1
- Update to 4.8.0

* Sun Jan 02 2011 Christoph Wickert <cwickert@fedoraproject.org> - 4.7.7-1
- Update to 4.7.7

* Sun Dec 19 2010 Christoph Wickert <cwickert@fedoraproject.org> - 4.7.6-2
- Own %%{_libexecdir}/xfce4/panel-plugins/ for now

* Sun Dec 05 2010 Christoph Wickert <cwickert@fedoraproject.org> - 4.7.6-1
- Update to 4.7.6

* Sat Dec 04 2010 Christoph Wickert <cwickert@fedoraproject.org> - 4.7.5-1
- Update to 4.7.5

* Mon Nov 08 2010 Christoph Wickert <cwickert@fedoraproject.org> - 4.7.4-1
- Update to 4.7.4

* Sun Sep 05 2010 Christoph Wickert <cwickert@fedoraproject.org> - 4.7.3-1
- Update to 4.7.3
- Update icon-cache scriptlets

* Fri May 21 2010 Kevin Fenzi <kevin@tummy.com> - 4.6.4-1
- Update to 4.6.4

* Sat Feb 13 2010 Kevin Fenzi <kevin@tummy.com> - 4.6.3-2
- Add patch for DSO fix. Fixes bug 564694

* Wed Dec 30 2009 Christoph Wickert <cwickert@fedoraproject.org> - 4.6.3-1
- Update to 4.6.3

* Fri Oct 16 2009 Christoph Wickert <cwickert@fedoraproject.org> - 4.6.2-1
- Update to 4.6.2
- Drop explicit requires on Terminal and mousepad

* Wed Sep 30 2009 Christoph Wickert <cwickert@fedoraproject.org> - 4.6.1-4
- Add xfswitch-plugin to default panel config (#525563)

* Mon Jul 27 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.6.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Fri Jun 12 2009 Christoph Wickert <cwickert@fedoraproject.org> - 4.6.1-2
- Bring back the multilib patch to fix #505165

* Sun Apr 19 2009 Kevin Fenzi <kevin@tummy.com> - 4.6.1-1
- Update to 4.6.1

* Sat Feb 28 2009 Christoph Wickert <cwickert@fedoraproject.org> - 4.6.0-2
- Fix directory ownership problems
- Require xfce4-doc
- Mark gtk-doc files as %%doc
- Obsolete the xfce4-xmms-plugin

* Thu Feb 26 2009 Kevin Fenzi <kevin@tummy.com> - 4.6.0-1
- Update to 4.6.0
- Remove some unneeded BuildRequires

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.5.99.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Jan 26 2009 Kevin Fenzi <kevin@tummy.com> - 4.5.99.1-1
- Update to 4.5.99.1

* Tue Jan 13 2009 Kevin Fenzi <kevin@tummy.com> - 4.5.93-1
- Update to 4.5.93

* Sat Dec 27 2008 Kevin Fenzi <kevin@tummy.com> - 4.5.92-1
- Update to 4.5.92

* Mon Oct 27 2008 Christoph Wickert <cwickert@fedoraproject.org> - 4.4.3-1
- Update to 4.4.3
- Remove mailwatch-plugin from default panel config again
- BuildRequire intltool
- Update gtk-update-icon-cache scriptlets
- Fix BuildRoot tag

* Thu Oct 02 2008 Christoph Wickert <cwickert@fedoraproject.org> - 4.4.2-5
- Fix FTBFS (#465058)
- Update defaults patch to include mailwatch plugin
- Remove old xfce4-iconbox and xftaskbar dummy files

* Tue Apr 08 2008 Kevin Fenzi <kevin@tummy.com> - 4.4.2-4
- Add defaults patch. See bug 433573

* Sat Feb 23 2008 Kevin Fenzi <kevin@tummy.com> - 4.4.2-3
- Drop dependency on xfce-icon-theme. See bug 433152

* Sun Feb 10 2008 Kevin Fenzi <kevin@tummy.com> - 4.4.2-2
- Rebuild for gcc43

* Sun Dec  2 2007 Kevin Fenzi <kevin@tummy.com> - 4.4.2-1
- Update to 4.4.2 (fixes 382471)
- Drop Provides/Obsoletes for xfce4-modemlights-plugin to come back.

* Mon Aug 27 2007 Kevin Fenzi <kevin@tummy.com> - 4.4.1-4
- Update License tag

* Mon Jul 30 2007 Kevin Fenzi <kevin@tummy.com> - 4.4.1-3
- Own %%{_datadir}/xfce4/

* Wed Jun  6 2007 Kevin Fenzi <kevin@tummy.com> - 4.4.1-2
- Fix multilib issues. Bug #228168

* Wed Apr 11 2007 Kevin Fenzi <kevin@tummy.com> - 4.4.1-1
- Upgrade to 4.4.1

* Tue Apr  3 2007 Kevin Fenzi <kevin@tummy.com> - 4.4.0-2
- Own %%{_libexecdir}/xfce4/
- Do not own %%{_libdir}/xfce4/mcs-plugins

* Sun Jan 21 2007 Kevin Fenzi <kevin@tummy.com> - 4.4.0-1
- Upgrade to 4.4.0

* Sat Nov 11 2006 Kevin Fenzi <kevin@tummy.com> - 4.3.99.2-2
- Require xfce4-icon-theme. 

* Thu Nov  9 2006 Kevin Fenzi <kevin@tummy.com> - 4.3.99.2-1
- Update to 4.3.99.2

* Fri Oct  6 2006 Kevin Fenzi <kevin@tummy.com> - 4.3.99.1-7
- List full old versions in Obsoletes

* Fri Oct  6 2006 Kevin Fenzi <kevin@tummy.com> - 4.3.99.1-6
- Tweak Obsolete versions

* Thu Oct  5 2006 Kevin Fenzi <kevin@tummy.com> - 4.3.99.1-5
- Add Requires libxfcegui4-devel to devel package

* Thu Oct  5 2006 Kevin Fenzi <kevin@tummy.com> - 4.3.99.1-4
- Add period to description
- Fix defattr
- Add gtk-update-icon-cache in post/postun

* Wed Oct  4 2006 Kevin Fenzi <kevin@tummy.com> - 4.3.99.1-3
- Add Requires for mousepad and Terminal
- Bump for devel checkin

* Sun Sep 24 2006 Kevin Fenzi <kevin@tummy.com> - 4.3.99.1-2
- Obsolete some more plugins who's functionality has been pulled in. 
- Own the libexecdir/xfce4/panel-plugins for new plugins. 

* Sun Sep  3 2006 Kevin Fenzi <kevin@tummy.com> - 4.3.99.1-1
- Update to 4.3.99.1
- Add Provides/Obsoletes for xfce4-iconbox, xfce4-systray, xfce4-toys, xfce4-trigger-launcher
- Fix typo in devel subpackage summary
- Add post/postun ldconfig calls

* Thu Aug 24 2006 Kevin Fenzi <kevin@tummy.com> - 4.3.90.2-2
- fix .so in main package
- add Requires for libxfce4util-devel
- don't own includedir/xfce4 (libxfce4util-devel should)

* Tue Jul 11 2006 Kevin Fenzi <kevin@tummy.com> - 4.3.90.2
- Upgrade to 4.3.90.2

* Thu Apr 27 2006 Kevin Fenzi <kevin@tummy.com> - 4.3.90.1.fc6
- Upgrade to 4.3.90.1

* Thu Feb 16 2006 Kevin Fenzi <kevin@tummy.com> - 4.2.3-3.fc5
- Rebuild for fc5

* Tue Jan 31 2006 Kevin Fenzi <kevin@tummy.com> - 4.2.3-2.fc5
- added imake and libXt-devel BuildRequires for modular xorg

* Mon Nov  7 2005 Kevin Fenzi <kevin@tummy.com> - 4.2.3-1.fc5
- Update to 4.2.3
- Added dist tag
- Tweaked panel-htmlview patch to add - in translation

* Tue May 17 2005 Kevin Fenzi <kevin@tummy.com> - 4.2.2-1.fc4
- Update to 4.2.2

* Fri Mar 25 2005 Kevin Fenzi <kevin@tummy.com> - 4.2.1.1-4.fc4
- lowercase Release

* Thu Mar 24 2005 Kevin Fenzi <kevin@tummy.com> - 4.2.1.1-3.FC4
- Added htmlview patch
- Removed unneeded la/a files

* Sat Mar 19 2005 Warren Togami <wtogami@redhat.com> - 4.2.1.1-2
- remove stuff

* Thu Mar 17 2005 Kevin Fenzi <kevin@tummy.com> - 4.2.1.1-1
- Updated to 4.2.1.1 version
- Changed Requires/Buildrequires to 4.2.1, as xfce4-panel was the only package updated to 4.2.1.1

* Tue Mar 15 2005 Kevin Fenzi <kevin@tummy.com> - 4.2.1-1
- Updated to 4.2.1 version

* Tue Mar  8 2005 Kevin Fenzi <kevin@tummy.com> - 4.2.0-4
- Fixed case of Xfce

* Sun Mar  6 2005 Kevin Fenzi <kevin@tummy.com> - 4.2.0-3
- Created a new patch to change mozilla -mail to launchmail
- Moved the includes to the devel subpackage

* Thu Feb 03 2005 Than Ngo <than@redhat.com> 4.2.0-2
- new sub package xfce4-panel-devel

* Tue Jan 25 2005 Than Ngo <than@redhat.com> 4.2.0-1
- 4.2.0

* Wed Dec 08 2004 Than Ngo <than@redhat.com> 4.0.6-2 
- add patch to use lauchmail/htmlview #142160

* Mon Jul 19 2004 Than Ngo <than@redhat.com> 4.0.6-1
- update to 4.0.6
- remove some unneeded patch files

* Tue Jun 15 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Jun 01 2004 Than Ngo <than@redhat.com> 4.0.5-4
- add buildrequires on startup-notification-devel, bug #124948
- use %%find_lang macros, bug #124948

* Mon May 31 2004 Than Ngo <than@redhat.com> 4.0.5-3
- own %%{_libdir}i/xfce4, bug #124826

* Mon Apr 26 2004 Than Ngo <than@redhat.com> 4.0.5-2
- Change more defaults for fedora, use startup notification
  by default, remove "-splash" option from mozilla launcher. Thanks to Olivier Fourdan
- Patch to avoid crash at startup under some rare circumstances
- Change defaults for fedora

* Thu Apr 15 2004 Than Ngo <than@redhat.com> 4.0.5-1
- update to 4.0.5

* Tue Mar 02 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com>
- rebuilt

* Tue Jan 27 2004 Than Ngo <than@redhat.com> 4.0.3-2
- fixed dependant libraries check on x86_64

* Fri Jan 09 2004 Than Ngo <than@redhat.com> 4.0.3-1
- 4.0.3 release

* Thu Dec 25 2003 Than Ngo <than@redhat.com> 4.0.2-1
- 4.0.2 release

* Tue Dec 16 2003 Than Ngo <than@redhat.com> 4.0.1-1
- initial build

