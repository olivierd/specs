Name:		python-xlib
Version:	0.25
Release:	1%{?dist}
Summary:	X client library for Python

License:	LGPLv2+
URL:		https://github.com/python-xlib/python-xlib
Source0:	https://github.com/%{name}/%{name}/releases/download/%{version}/%{name}-%{version}.tar.bz2

BuildArch:	noarch

%description
The Python X Library is a complete X11R6 client-side implementation,
written in pure Python. It can be used to write low-levelish X Windows
client application in Python.

%package -n python2-xlib
Summary:	X client library for Python 2
BuildRequires:	python2-devel
BuildRequires:	python2-setuptools_scm
BuildRequires:	python2-six >= 1.10.0

Requires:       python2-six >= 1.10.0
%{?python_provide:%python_provide python2-xlib}

%description -n python2-xlib
The Python X Library is a complete X11R6 client-side implementation, 
written in pure Python. It can be used to write low-levelish X Windows 
client applications in Python 2

%package -n python%{python3_pkgversion}-xlib
Summary:	X client library for Python 3
BuildRequires:	python%{python3_pkgversion}-devel
BuildRequires:	python%{python3_pkgversion}-setuptools_scm
BuildRequires:	python%{python3_pkgversion}-six >= 1.10.0

Requires:       python%{python3_pkgversion}-six >= 1.10.0
%{?python_provide:%python_provide python%{python3_pkgversion}-xlib}

%description -n python%{python3_pkgversion}-xlib
The Python X Library is a complete X11R6 client-side implementation, 
written in pure Python. It can be used to write low-levelish X Windows 
client applications in Python 3.

%prep
%setup -q

%build
%py2_build
%py3_build

%install
%py2_install
%py3_install

%files -n python2-xlib
%license LICENSE
%doc CHANGELOG.md README.rst TODO
%{python2_sitelib}/*

%files -n python%{python3_pkgversion}-xlib
%license LICENSE
%doc CHANGELOG.md README.rst TODO
%{python3_sitelib}/*

%changelog
* Sat Mar 09 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.25
