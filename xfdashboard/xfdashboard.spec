%global major 0.9
%global libversion 0.900.90

Name:		xfdashboard
Version:	0.9.90
Release:	99%{?dist}
Summary:	GNOME shell like dashboard for Xfce

License:	GPLv2+
Source0:	https://archive.xfce.org/src/apps/xfdashboard/%{major}/%{name}-%{version}.tar.bz2

BuildRequires:	libtool
BuildRequires:	perl
BuildRequires:	intltool
BuildRequires:	gettext

BuildRequires:	pkgconfig(x11)
BuildRequires:	pkgconfig(xcomposite)
BuildRequires:	pkgconfig(xdamage)
BuildRequires:	pkgconfig(xinerama)

BuildRequires:	pkgconfig(libwnck-3.0)
BuildRequires:	pkgconfig(clutter-1.0)
BuildRequires:	pkgconfig(clutter-cogl-1.0)
BuildRequires:	pkgconfig(gtk+-3.0)
BuildRequires:	pkgconfig(glib-2.0)
BuildRequires:	pkgconfig(gio-2.0)
BuildRequires:	pkgconfig(gio-unix-2.0)
BuildRequires:	pkgconfig(garcon-1)
BuildRequires:	pkgconfig(libxfconf-0)
BuildRequires:	pkgconfig(libxfce4util-1.0)
BuildRequires:	pkgconfig(libxfce4ui-2)

BuildRequires:	desktop-file-utils
BuildRequires:	gtk-update-icon-cache

Requires:	libxfce4ui

%description
Xfdashboard provides a GNOME shell dashboard like interface for use with Xfce
desktop. It can be configured to run to any keyboard shortcut and when executed
provides an overview of applications currently open enabling the user to switch
between different applications. The search feature works like Xfce's app finder
which makes it convenient to search for and start applications.

%package themes
Summary:	Themes for Xfdashboard
Requires:	%{name}%{?_isa} = %{version}-%{release}


%description themes
Additional themes for use with Xfdashboard

%package devel
Summary:	Devel files for Xfdashboard
Requires:	%{name}%{?_isa} = %{version}-%{release}

%description devel
Development related files for Xfdashboard

%prep
%setup -q

# Remove symbolic icons
#find data/themes -type f -name 'xfdashboard.css' \
#	| xargs %{__sed} -i 's/"window-close-symbolic", // ;
#	s/"edit-find-symbolic", // ;
#	s/go-home-symbolic/go-home/ ;
#	s/"edit-clear-symbolic", //'
# Decrease saturation effect
find data/themes -type f -name 'effects.xml' \
	| xargs %{__sed} -i 's/factor">1.0/factor">0.45/'

%build
%configure

# Remove rpaths
%{__sed} -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
%{__sed} -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

%make_build

%install
%make_install

find %{buildroot} -name "*.la" | xargs %{__rm}

%find_lang %{name}

%check
desktop-file-validate %{buildroot}%{_datadir}/applications/org.xfce.%{name}.desktop
desktop-file-validate %{buildroot}%{_datadir}/applications/org.xfce.%{name}-settings.desktop
desktop-file-validate %{buildroot}%{_sysconfdir}/xdg/autostart/org.xfce.%{name}-autostart.desktop

%ldconfig_scriptlets devel

%post
touch --no-create %{_datadir}/icons/hicolor &> /dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
	gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%license COPYING
%doc AUTHORS README.md
%{_bindir}/%{name}
%{_bindir}/%{name}-settings
%{_datadir}/%{name}/bindings.xml
%{_datadir}/%{name}/preferences.ui
%{_datadir}/metainfo/org.xfce.%{name}.metainfo.xml
%{_datadir}/applications/org.xfce.%{name}.desktop
%{_datadir}/applications/org.xfce.%{name}-settings.desktop
%{_sysconfdir}/xdg/autostart/org.xfce.%{name}-autostart.desktop
%{_datadir}/icons/hicolor/*/*/*
%{_datadir}/themes/%{name}/*
%{_libdir}/lib%{name}.so.0
%{_libdir}/lib%{name}.so.%{libversion}
%{_libdir}/%{name}/plugins/autopin-windows.so
%{_libdir}/%{name}/plugins/clock-view.so
%{_libdir}/%{name}/plugins/gnome-shell-search-provider.so
%{_libdir}/%{name}/plugins/hot-corner.so
%{_libdir}/%{name}/plugins/middle-click-window-close.so
%{_libdir}/%{name}/plugins/recently-used-search-provider.so

%files themes
%defattr(-,root,root,-)
%{_datadir}/themes/%{name}-*/*

%files devel
%defattr(-,root,root,-)
%{_includedir}/%{name}/*
%{_libdir}/pkgconfig/lib%{name}.pc
%{_libdir}/lib%{name}.so

%changelog
* Fri Mar 04 2022 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.9.90

* Sat Oct 02 2021 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.9.5

* Sun Sep 26 2021 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.9.4

* Sat Mar 06 2021 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.8.1

* Sat Dec 26 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.8.0

* Sun Nov 22 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.7.90

* Wed Sep 23 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.7.8

* Tue Dec 03 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.7.7

* Thu Aug 09 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.7.5

* Tue Sep 19 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.7.4

* Sat Sep 16 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Decrease saturation effect

* Fri Aug 25 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Fix quicklaunch buttons with xfconf 4.13

* Sat Jul 29 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Remove symbolic icons in .css files, decrease their size

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.7.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Thu Jul 20 2017 Kevin Fenzi <kevin@scrye.com> - 0.7.3-1
- update to 0.7.3. Fixes bug #1469327

* Mon May 15 2017 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.7.2-1
- Update to 0.7.2

* Tue Mar 07 2017 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.7.1-1
- Update to 0.7.1

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.7.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Wed Sep 07 2016 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.7.0-1
- Update to 0.7.0

* Sat May 21 2016 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.6.0-1
- Update to 0.6.0

* Sat Apr 16 2016 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.5.92-3
- Fix license file installation

* Thu Mar 31 2016 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.5.92-2
- Update to correct libxfdashboard version
- Add macro for library versioning

* Thu Mar 31 2016 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.5.92-1
- Bug fix update
- Minor UI change (configure button for plugins)

* Thu Mar 24 2016 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.5.91-3
- Enabled live windows and multi-monitor support
- added BR: libxcomposite, libxinerama, libxdamage

* Tue Mar 22 2016 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.5.91-2
- Update library version and fix typos

* Tue Mar 22 2016 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.5.91-1
- Update to v0.5.91
- Drop upstreamed patch

* Sun Mar 06 2016 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.5.90-3
- Fix bungled requires for devel subpackage

* Sat Mar 05 2016 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.5.90-2
- Add upstream patch to enable hot fix initialization

* Sat Mar 05 2016 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.5.90-1
- Update to latest upstream version
- Added devel subpackage
- Added xfdashboard libraries

* Fri Feb 05 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.5.5-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Sun Jan 31 2016 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.5.5-1
- Update to 0.5.5
- Fixes bug #1303479

* Sat Dec 19 2015 Kevin Fenzi <kevin@scrye.com> - 0.5.4-1
- Update to 0.5.4. Fixes bug #1293031

* Sat Nov 14 2015 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.5.3-1
- Update to latest upstream version (0.5.3)

* Thu Oct 08 2015 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.5.2-1
- Update to 0.5.2

* Tue Sep 08 2015 Kevin Fenzi <kevin@scrye.com> 0.5.1-1
- Update to 0.5.1. Fixes bug #1260818

* Fri Jun 19 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Thu Jun 11 2015 Kevin Fenzi <kevin@scrye.com> 0.5.0-1
- Update to 0.5.0

* Sat May 30 2015 Kevin Fenzi <kevin@scrye.com> 0.4.2-1
- Update to 0.4.2

* Sat May 30 2015 Kevin Fenzi <kevin@scrye.com> 0.4.1-1
- Update to 0.4.1 Fixes bug #1226500

* Sun Apr 12 2015 Kevin Fenzi <kevin@scrye.com> 0.4.0-1
- Update to 0.4.0 Fixes bug #1211005

* Sat Mar 28 2015 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.3.91-1
- Update to 0.3.91
- Removed upstreamed patches
- Updated to source and URL to point to xfce project links

* Thu Mar 26 2015 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.3.90-2
- Fix for crash due to missing icons

* Wed Mar 18 2015 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.3.90-1
- Update to 0.3.90
- Removed patch for forcing X11 backend in clutter

* Mon Mar 09 2015 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.3.9-4
- Force X11 backend for clutter

* Sun Mar 01 2015 Mukundan Ragavan <nonamedotc@gmail.com> - 0.3.9-3
- Rebuild from Xfce 4.12

* Sat Feb 21 2015 Till Maas <opensource@till.name> - 0.3.9-2
- Rebuilt for Fedora 23 Change
  https://fedoraproject.org/wiki/Changes/Harden_all_packages_with_position-independent_code

* Sat Feb 21 2015 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.3.9-1
- Updated to 0.3.9
- Translatations added

* Sat Jan 24 2015 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.3.8-1
- Update to 0.3.8

* Sat Jan 17 2015 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.3.7-1
- Update to 0.3.7
- Bugfix update to 0.3.6

* Sat Jan 17 2015 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.3.6-1
- Update to 0.3.6
- Added libxfce4util-devel as BR for building settings manager

* Mon Jan 12 2015 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.3.5-1
- Update to 0.3.5

* Thu Nov 27 2014 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.3.4-1
- Update to 0.3.4
- Move additional themes provided to -themes subpackage

* Mon Oct 20 2014 Kevin Fenzi <kevin@scrye.com> 0.3.3-1
- Update to 0.3.3

* Mon Sep 08 2014 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.3.2-1
- Updated to latest upstream version
- New features included

* Wed Sep 03 2014 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.3.1-1
- Updated to latest upstream version

* Mon Aug 18 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Tue Jul 22 2014 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.3.0-1
- Latest upstream version with bugfixes and enhancements
- Removed ChangeLog from doc

* Sun Jul 20 2014 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.2.1-1
- Updated to latest upstream version

* Sat Jul 12 2014 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.2.0-2
- Initial build for F22

* Wed Jun 11 2014 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.2.0-1
- Updated to latest upstream release
- Added appdata file

* Wed Jun 04 2014 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.1.92-1
- Updated to latest version
- Added application icons

* Tue May 20 2014 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.1.91-1
- Fixed URL
- Updated to 0.1.91 - API change with this update
- Removed TODO
- Added BuildRequires for desktop-file-utils
- desktop file added - will show in XFCE menu
- desktop file for autostart added

* Fri May 02 2014 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.1.90-1
- Update to 0.1.90
- multiple bug fixes and improvements

* Sun Mar 23 2014 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.1.6-2
- Updated to 0.1.6
- Added xfdashboard.xml to files section

* Sun Mar 23 2014 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.1.6-1
- Updated to 0.1.6

* Sat Mar 8 2014 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.1.5-1
- Updated to latest release containing new enhancements

* Tue Feb 25 2014 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.1.4-2
- Patch for enabling workspace switching added
- Upstream bug - issue#1 on github

* Mon Feb 24 2014 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.1.4-1
- Updated to the latest upstream version
- Includes theming support (provides a default theme)

* Tue Feb 11 2014 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.1.3-1
- Updated to the latest upstream version

* Mon Feb 10 2014 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.1.2-3
- Corrected flags used for building the package
- Edited the description

* Sun Feb 02 2014 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.1.2-2
- Corrected version used for packaging
- Added the doc files

* Wed Jan 29 2014 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.1.3-1
- Initial build for Fedora
