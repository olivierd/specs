%global major 4.17

Name:	libxfce4ui
Version:	4.17.6
Release:	99%{?dist}
Summary:	Commonly used Xfce widgets

License:	LGPLv2+
URL:        https://xfce.org/
Source0:	https://archive.xfce.org/src/xfce/%{name}/%{major}/%{name}-%{version}.tar.bz2

BuildRequires:	libtool
BuildRequires:	gettext
BuildRequires:	intltool

BuildRequires:	vala
BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(gobject-2.0)
BuildRequires:  pkgconfig(gtk+-3.0) >= 3.22.0
BuildRequires:  pkgconfig(libxfce4util-1.0) >= 4.15.6
BuildRequires:	pkgconfig(libxfconf-0) >= 4.15.0
BuildRequires:  pkgconfig(gobject-introspection-1.0)
BuildRequires:  pkgconfig(libstartup-notification-1.0)
BuildRequires:	pkgconfig(x11)
BuildRequires:	pkgconfig(sm)
BuildRequires:	pkgconfig(gladeui-2.0)
BuildRequires:	pkgconfig(libgtop-2.0)
BuildRequires:	pkgconfig(epoxy)
BuildRequires:	pkgconfig(gudev-1.0)
BuildRequires:  desktop-file-utils
BuildRequires:  gtk-update-icon-cache

Requires:	perl

%description
Libxfce4ui is used to share commonly used Xfce widgets among the Xfce
applications.

%package	devel
Summary:	Development files for %{name}
Requires:	%{name}%{?_isa} = %{version}-%{release}
Requires:	gtk3-devel
Requires:	glade-devel
Requires:   gobject-introspection-devel
Requires:	vala
Requires:	libxfce4util-devel >= 4.15.6

%description	devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q

%build
%configure --disable-static --enable-startup-notification \
	--enable-introspection=yes \
    --enable-vala \
	--enable-gladeui2=yes \
	--without-html-dir \
    --enable-glibtop \
    --enable-epoxy \
	--with-vendor-info=Fedora

# Remove rpaths
%{__sed} -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
%{__sed} -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

# Needed for gobject-introspection build, because we killed RPATH
export LD_LIBRARY_PATH=$( pwd )/libxfce4ui/.libs:${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}

%make_build

%install
%make_install

# fix permissions for installed libraries
%{__chmod} 755 %{buildroot}/%{_libdir}/*.so

find %{buildroot} -name '*.la' | xargs %{__rm}

%find_lang %{name}

%ldconfig_scriptlets devel

%post
update-desktop-database &> /dev/null ||:
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
update-desktop-database &> /dev/null ||:
if [ $1 -eq 0 ] ; then
	touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS NEWS THANKS
%license COPYING
%config(noreplace) %{_sysconfdir}/xdg/xfce4/xfconf/xfce-perchannel-xml/xfce4-keyboard-shortcuts.xml
%{_libdir}/libxfce4kbd-private-3.so.*
%{_libdir}/libxfce4ui-2.so.*
%{_bindir}/xfce4-about
%{_datadir}/applications/xfce4-about.desktop
%{_datadir}/icons/hicolor/*/*/*

%files devel
%defattr(-,root,root,-)
%doc TODO
%{_includedir}/xfce4/libxfce4kbd-private-3/libxfce4kbd-private/*
%{_includedir}/xfce4/libxfce4ui-2/libxfce4ui/*
%{_libdir}/girepository-1.0/Libxfce4ui-2.0.typelib
%{_libdir}/libxfce4kbd-private-3.so
%{_libdir}/libxfce4ui-2.so
%{_libdir}/pkgconfig/libxfce4kbd-private-3.pc
%{_libdir}/pkgconfig/libxfce4ui-2.pc
%{_libdir}/glade/modules/libxfce4uiglade2.so
%{_datadir}/gir-1.0/Libxfce4ui-2.0.gir
%{_datadir}/glade/catalogs/libxfce4ui-2.xml
%{_datadir}/glade/pixmaps/hicolor/*/*/widget-libxfce4ui*.png
%{_datadir}/vala/vapi/libxfce4ui-2.*

%changelog
* Sun Apr 03 2022 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.17.6

* Wed Mar 16 2022 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.17.5

* Tue Feb 22 2022 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.17.4

* Wed Dec 29 2021 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.17.3

* Sat Dec 11 2021 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.17.2

* Sun Sep 12 2021 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.17.1

* Sun Jun 27 2021 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.17.0

* Sat Dec 26 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.16.0

* Thu Dec 17 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.15.8

* Mon Dec 14 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.15.7

* Fri Dec 11 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.15.6

* Sun Nov 22 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.15.5

* Tue Nov 03 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.15.4

* Mon Jun 08 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.15.3

* Sun Apr 05 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.15.2

* Fri Jan 24 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.15.1

* Thu Dec 05 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.15.0

* Mon Aug 12 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.14.1

* Sun Jul 28 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.13.7

* Mon Jul 01 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.13.6

* Sat Jun 22 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Rebuild for Fedora 30

* Fri May 17 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.13.5

* Tue Nov 06 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Sync with Fedora 29

* Fri Dec 15 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.13.4

* Sun Jul 09 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.13.3

* Fri Feb 10 2017 Fedora Release Engineering <releng@fedoraproject.org> - 4.12.1-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Sat Jul 23 2016 Kevin Fenzi <kevin@scrye.com> - 4.12.1-5
- Drop control-alt-del for logout shortcut. Fixes bug #1229218

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 4.12.1-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Wed Jun 17 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.12.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Wed Apr 22 2015 Kevin Fenzi <kevin@scrye.com> 4.12.1-2
- Obsolete libxfcegui4.
- Drop amixer for volume buttons should be handled by xfce4-pulseaudio-plugin now.
- Fixes bug #1211313

* Sun Mar 15 2015 Kevin Fenzi <kevin@scrye.com> 4.12.1-1
- Update to 4.12.1 with various bugfixes.

* Sun Mar 1 2015 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 4.12.0-2
- Add shortcuts patch

* Sat Feb 28 2015 Mukundan Ragavan <noamedotc@fedoraproject.org> - 4.12.0-1
- Update to latest stable release 4.12.0
- Remove upstreamed patches
- Built with GTK3 enabled
- change Terminal to xfce4-terminal in shortcuts patch

* Sat Feb 21 2015 Till Maas <opensource@till.name> - 4.10.0-14
- Rebuilt for Fedora 23 Change
  https://fedoraproject.org/wiki/Changes/Harden_all_packages_with_position-independent_code

* Sun Aug 17 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.10.0-13
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.10.0-12
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Fri May 16 2014 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 4.10.0-11
- patch to fix bug #1095362
- patch13 - enable-shortcut.patch

* Fri May 16 2014 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 4.10.0-10
- Add patches to fix bug #1095362
- Patch11 - enable-shift-modifier-in-shortcut-dialog.patch
- Patch12 - enable-shift-modifier-in-shortcut-grabber.patch

* Wed Nov 06 2013 Kevin Fenzi <kevin@scrye.com> 4.10.0-9
- Add patch to fix double fork issue that prevents some apps from running from the menu. 
- Fixes bug #879922

* Wed Nov 06 2013 Kevin Fenzi <kevin@scrye.com> 4.10.0-8
- Conditionalize glade3 stuff so we can keep the same spec over branches. 

* Wed Nov 06 2013 Lubomir Rintel <lkundrak@v3.sk> - 4.10.0-7
- Re-enable glade3 module, it works now

* Sat Aug 03 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.10.0-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.10.0-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Tue Jan 22 2013 Christoph Wickert <cwickert@fedoraproject.org> - 4.10.0-4
- Don't ship xfce4-about in libxfce4ui-devel (#902537)

* Sat Oct 13 2012 Christoph Wickert <cwickert@fedoraproject.org> - 4.10.0-3
- Make a separate package for xfce4-about
- Add more keyboard shortcuts
- Move xfce4-about menu entry to 'Documentation' category

* Thu Jul 19 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.10.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Sat Apr 28 2012 Christoph Wickert <cwickert@fedoraproject.org> - 4.10.0-1
- Update to 4.10.0 final

* Sat Apr 14 2012 Kevin Fenzi <kevin@scrye.com> - 4.9.2-1
- Update to 4.9.2 (Xfce 4.10pre2)

* Sun Apr 01 2012 Kevin Fenzi <kevin@scrye.com> - 4.9.1-1
- Update to 4.9.1

* Fri Jan 13 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.8.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Fri Dec 23 2011 Christoph Wickert <cwickert@fedoraproject.org> - 4.8.1-1
- Update to 4.8.1

* Sun Dec 18 2011 Christoph Wickert <cwickert@fedoraproject.org> - 4.8.0-6
- Fix Control shortcuts (#768704)
- Add review # and VCS key

* Tue Dec 06 2011 Adam Jackson <ajax@redhat.com> - 4.8.0-5
- Rebuild for new libpng

* Wed Mar 16 2011 Christoph Wickert <cwickert@fedoraproject.org> - 4.8.0-5
- Remove requirements for libxfcegui4-devel now that glade support was omitted

* Tue Feb 22 2011 Rakesh Pandit <rakesh@fedoraproject.org> - 4.8.0-4
- Disable glade as it still uses old API

* Tue Feb 22 2011 Rakesh Pandit <rakesh@fedoraproject.org> - 4.8.0-3
- Rebuild for new glade

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.8.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sun Jan 16 2011 Kevin Fenzi <kevin@tummy.com> - 4.8.0-1
- Update to 4.8.0 final. 

* Sun Jan 02 2011 Christoph Wickert <cwickert@fedoraproject.org> - 4.7.6-1
- Update to 4.7.6

* Mon Nov 29 2010 Christoph Wickert <cwickert@fedoraproject.org> - 4.7.5-1
- Update to 4.7.5

* Mon Nov 08 2010 Christoph Wickert <cwickert@fedoraproject.org> - 4.7.4-1
- Update to 4.7.4

* Sun Sep 05 2010 Christoph Wickert <cwickert@fedoraproject.org> - 4.7.3-1
- Update to 4.7.3
- Drop dependency on gtk-doc (#604399)

* Tue Jul 27 2010 Christoph Wickert <cwickert@fedoraproject.org> - 4.7.2-2
- Fix file conflict with libxfce4gui (#618719)

* Fri May 21 2010 Christoph Wickert <cwickert@fedoraproject.org> - 4.7.2-1
- Update to 4.7.2

* Wed May 19 2010 Kevin Fenzi <kevin@tummy.com> - 4.7.1-3
- Rebuild for new glade version. 

* Tue Jan 12 2010 Christoph Wickert <cwickert@fedoraproject.org> - 4.7.1-2
- Fix license tag
- Build gtk-doc

* Tue Jan 05 2010 Christoph Wickert <cwickert@fedoraproject.org> - 4.7.1-1
- Initial spec file, based on libxfcegui4.spec

