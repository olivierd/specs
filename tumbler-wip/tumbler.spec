%global major 0.3

Name:           tumbler
Version:        0.3.0.6
Release:        100%{?dist}
Summary:        D-Bus service for applications to request thumbnails

License:        GPLv2+ and LGPLv2+
Source0:        https://archive.xfce.org/src/xfce/tumbler/%{major}/%{name}-%{version}.tar.bz2

##Patch0:	tumbler.rc.patch

BuildRequires:	libtool
BuildRequires:  intltool
BuildRequires:  gettext

BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(gio-2.0)
BuildRequires:  pkgconfig(gmodule-2.0)
BuildRequires:  pkgconfig(gthread-2.0)
# plugins
BuildRequires:  pkgconfig(gdk-pixbuf-2.0)
BuildRequires:	pkgconfig(freetype2)
BuildRequires:	pkgconfig(libpng)
BuildRequires:  pkgconfig(libjpeg)
BuildRequires:  pkgconfig(poppler-glib)
BuildRequires:  pkgconfig(librsvg-2.0)

# extra plugins
BuildRequires:	pkgconfig(libgsf-1)
BuildRequires:  pkgconfig(gstreamer-1.0)
BuildRequires:	pkgconfig(gstreamer-tag-1.0)
BuildRequires:  pkgconfig(libgepub-0.6)
BuildRequires:  pkgconfig(libwebp)

%description
Tumbler is a D-Bus service for applications to request thumbnails for various
URI schemes and MIME types. It is an implementation of the thumbnail
management D-Bus specification described on
http://live.gnome.org/ThumbnailerSpec written in an object-oriented fashion

Additional thumbnailers can be found in the tumbler-extras package

%package extras
Summary:       Additional thumbnailers for %{name}
Requires:      %{name}%{?_isa} = %{version}-%{release}

%description extras
This package contains additional thumbnailers for file types, which are not used
very much and require additional libraries to be installed.

%package devel
Summary:       Development files for %{name}
Requires:      %{name}%{?_isa} = %{version}-%{release}
Requires:      glib2-devel

%description devel
This package contains libraries and header files for developing applications 
that use %{name}.

%prep
%setup -q

%build
%configure \
	--disable-static \
	--disable-raw-thumbnailer \
	--disable-ffmpeg-thumbnailer \
	--disable-cover-thumbnailer \
	--without-html-dir

# Remove rpaths.
%{__sed} -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
%{__sed} -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

%make_build

%install
%make_install

find %{buildroot} -name "*.la" | xargs %{__rm}

%find_lang %{name}

%ldconfig_scriptlets devel

%files -f %{name}.lang
%defattr(-,root,root,-)
%license COPYING
%doc AUTHORS ChangeLog NEWS README
%config(noreplace) %{_sysconfdir}/xdg/tumbler/
%{_datadir}/dbus-1/services/org.xfce.Tumbler.*.service
%{_libdir}/libtumbler-1.so.*
%{_libdir}/tumbler-1/tumblerd
%{_libdir}/tumbler-1/plugins/cache/*
%{_libdir}/tumbler-1/plugins/tumbler-desktop-thumbnailer.so
%{_libdir}/tumbler-1/plugins/tumbler-font-thumbnailer.so
%{_libdir}/tumbler-1/plugins/tumbler-jpeg-thumbnailer.so
%{_libdir}/tumbler-1/plugins/tumbler-pixbuf-thumbnailer.so
%{_libdir}/tumbler-1/plugins/tumbler-poppler-thumbnailer.so

%files extras
%defattr(-,root,root,-)
%{_libdir}/tumbler-1/plugins/tumbler-gst-thumbnailer.so
%{_libdir}/tumbler-1/plugins/tumbler-odf-thumbnailer.so
%{_libdir}/tumbler-1/plugins/tumbler-gepub-thumbnailer.so
%{_libdir}/tumbler-1/plugins/tumbler-rsvg-thumbnailer.so
%{_libdir}/tumbler-1/plugins/tumbler-webp-thumbnailer.so

%files devel
%defattr(-,root,root,-)
%doc TODO
%{_includedir}/*
%{_libdir}/libtumbler-1.so
%{_libdir}/pkgconfig/tumbler-1.pc

%changelog
* Wed Nov 11 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Add librsvg plugin
- Add support of animated images in libwebp plugin

* Wed Oct 28 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Add libwebp plugin

* Tue Sep 29 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Add libgepub plugin (for .epub files)

* Thu Aug 13 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.3.0

* Fri Aug 07 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.2.9

* Sat Dec 21 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.2.8

* Mon Aug 12 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.2.7

* Sun Jun 30 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.2.5

* Fri Jun 07 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Add support of librsvg

* Fri May 17 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.2.4

* Thu Sep 13 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.2.3

* Sat Sep 08 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.2.2

* Sat Mar 31 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.2.1

* Mon Jul 17 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.2.0

* Sat Jul 15 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.1.92.1

* Sat Jun 10 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.1.32

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.1.31-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Fri Dec 30 2016 Kevin Fenzi <kevin@scrye.com> - 0.1.31-5
- Conditionalize libopenraw patch for f26 and newer only

* Sat Dec 03 2016 Kevin Fenzi <kevin@scrye.com> - 0.1.31-4
- Rebuild for new libopenrawgnome

* Fri Feb 05 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.1.31-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Fri Jun 19 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.31-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Sat Feb 28 2015 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.1.31-1
- Update to version 0.1.31

* Mon Aug 18 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.30-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Sun Jun 08 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.30-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Wed Feb 26 2014 Kevin Fenzi <kevin@scrye.com> 0.1.30-1
- Update to 0.1.30

* Sun Aug 04 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.29-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Sun May 05 2013 Kevin Fenzi <kevin@scrye.com> 0.1.29-1
- Update to 0.1.29

* Sun May 05 2013 Kevin Fenzi <kevin@scrye.com> 0.1.28-1
- Update to 0.1.28

* Sun Mar 31 2013 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.27-3
- Make -extras package for GStreamer and RAW plugins
- Mark tumbler.rc as %%config(noreplace)

* Sun Mar 24 2013 Kevin Fenzi <kevin@scrye.com> 0.1.27-2
- Modify gstreamer BuildRequires (fixes bug #927011)
- Add new tumbler.rc file.

* Sun Mar 17 2013 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.27-1
- Update to 0.1.27

* Fri Feb 15 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.26-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Mon Jan 21 2013 Adam Tkac <atkac redhat com> - 0.1.26-2
- rebuild due to "jpeg8-ABI" feature drop

* Sun Dec 09 2012 Kevin Fenzi <kevin@scrye.com> 0.1.26-1
- Update to 0.1.26

* Sun Jul 22 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.25-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Wed May 16 2012 Marek Kasik <mkasik@redhat.com> - 0.1.25-2
- Rebuild (poppler-0.20.0)

* Sun Apr 29 2012 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.25-1
- Update to 0.1.25 (Xfce 4.10 final)
- Add VCS key

* Tue Apr 03 2012 Kevin Fenzi <kevin@scrye.com> - 0.1.24-1
- Update to 0.1.24

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.23-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Thu Dec 08 2011 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.23-1
- Update to 0.1.23

* Tue Dec 06 2011 Adam Jackson <ajax@redhat.com> - 0.1.22-5
- Rebuild for new libpng

* Sun Oct 16 2011 Christoph Wickert <cwickert@fedoraproject.org> - 1.22-4
- Fix thumbnail generation of the GStreamer plugin (#746110)
- Fix ownership race conditions when started twice (bugzilla.xfce.org #8001)

* Fri Sep 30 2011 Marek Kasik <mkasik@redhat.com> - 1.22-3
- Rebuild (poppler-0.18.0)

* Wed Sep 21 2011 Christoph Wickert <cwickert@fedoraproject.org> - 1.22-2
- Build the new GStreamer video thumbnailer

* Wed Sep 21 2011 Christoph Wickert <cwickert@fedoraproject.org> - 1.22-1
- Update to 1.22

* Mon Sep 19 2011 Marek Kasik <mkasik@redhat.com> - 1.21-3
- Rebuild (poppler-0.17.3)

* Fri Jul 15 2011 Marek Kasik <mkasik@redhat.com> - 1.21-2
- Rebuild (poppler-0.17.0)

* Mon Feb 14 2011 Christoph Wickert <cwickert@fedoraproject.org> - 1.21-1
- Update to 1.21

* Sat Feb 12 2011 Christoph Wickert <cwickert@fedoraproject.org> - 1.20-1
- Update to 1.20

* Wed Feb 09 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.6-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sun Jan 16 2011 Kevin Fenzi <kevin@tummy.com> - 0.1.6-1
- Update to 0.1.6

* Sun Dec 05 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.5-1
- Update to 0.1.5

* Thu Nov 18 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.4-1
- Update to 0.1.4

* Wed Nov 03 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.3-1
- Update to 0.1.3
- Enable PDF thumbnails (BR poppler-glib-devel)

* Sat Jul 17 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.2-1
- Update to 0.1.2
- Own %%{_datadir}/gtk-doc/{html/} (#604169)
- Include NEWS in %%doc

* Thu Feb 25 2010 Rahul Sundaram <sundaram@fedoraproject.org> - 0.1.1-2
- Fix thumbnail support by including necessary BR's

* Fri Jan 15 2010 Debarshi Ray <rishi@fedoraproject.org> - 0.1.1-1
- Version bump to 0.1.1.
  * New fast JPEG thumbnailer with EXIF support
  * Report unsupported flavors back to clients via error signals
  * Translation updates: Swedish, Catalan, Galician, Japanese, Danish,
    Portuguese, Chinese
- Added 'BuildRequires: gtk2-devel'.
- Use sed instead of chrpath to remove rpaths. Remove 'BuildRequires: chrpath'.

* Tue Dec 22 2009 Rahul Sundaram <sundaram@fedoraproject.org> - 0.1.0-2
- Updated spec for review

* Sun Dec 20 2009 Debarshi Ray <rishi@fedoraproject.org> - 0.1.0-1
- Initial build.
