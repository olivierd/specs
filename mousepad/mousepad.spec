%global major 0.5

Name:           mousepad
Version:        0.5.9
Release:        99%{?dist}
Summary:        Simple text editor for Xfce desktop environment

Group:          Applications/Editors
License:        GPLv2+
URL:            http://xfce.org/
Source0:        https://archive.xfce.org/src/apps/%{name}/%{major}/%{name}-%{version}.tar.bz2

BuildRequires:	libtool
BuildRequires:	perl
BuildRequires:	intltool
BuildRequires:	gettext

BuildRequires:	pkgconfig(glib-2.0)
BuildRequires:	pkgconfig(gthread-2.0)
BuildRequires:	pkgconfig(gtk+-3.0)
BuildRequires:	pkgconfig(gtksourceview-4)
# gspell-plugin
BuildRequires:	pkgconfig(gspell-1)
Requires:       hunspell-fr
# shortcuts plugin
BuildRequires:	pkgconfig(libxfce4ui-2) >= 4.17.5

Requires:       glib2
# Store user settings
Requires:       dconf
BuildRequires:  desktop-file-utils
BuildRequires:	libappstream-glib

%description
Mousepad aims to be an easy-to-use and fast editor. It's target is an editor for
quickly editing text files, not a development environment or an editor with a
huge bunch of plugins.

Mousepad is based on Leafpad. The initial reason for Mousepad was to provide
printing support, which would have been difficult for Leafpad for various
reasons.

Although some features are under development, currently Mousepad has following
features:

    * Complete support for UTF-8 text
    * Cut/Copy/Paste and Select All text
    * Search and Replace
    * Font selecton
    * Word Wrap
    * Character coding selection
    * Auto character coding detection (UTF-8 and some codesets)
    * Manual codeset setting
    * Infinite Undo/Redo by word
    * Auto Indent
    * Multi-line Indent
    * Display line numbers
    * Drag and Drop
    * Printing

%prep
%setup -q

# French is default language
%{__sed} -i 's|en_US|fr_FR|' \
    plugins/gspell-plugin/org.xfce.mousepad.plugins.gspell.gschema.xml

%build
%configure --disable-static \
    --enable-gtksourceview4 \
    --enable-keyfile-settings \
    --enable-plugin-gspell \
    --enable-plugin-shortcuts \
    --enable-plugin-skeleton

%make_build

%install
%make_install

find %{buildroot} -name '*.la' | xargs %{__rm}

%find_lang mousepad

%check
appstream-util validate-relax --nonet \
	%{buildroot}%{_datadir}/metainfo/org.xfce.mousepad.appdata.xml

%post
update-desktop-database &> /dev/null ||:

%postun
update-desktop-database &> /dev/null ||:
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    glib-compile-schemas --allow-any-name %{_datadir}/glib-2.0/schemas &> /dev/null || :
fi

%posttrans
glib-compile-schemas --allow-any-name %{_datadir}/glib-2.0/schemas &> /dev/null || :

%files -f mousepad.lang
%defattr(-,root,root,-)
%doc AUTHORS NEWS
%license COPYING
%{_bindir}/mousepad
%{_libdir}/libmousepad.so*
%{_libdir}/mousepad/plugins/libmousepad-plugin-gspell.so
%{_libdir}/mousepad/plugins/libmousepad-plugin-shortcuts.so
%{_metainfodir}/org.xfce.mousepad.appdata.xml
%{_datadir}/applications/org.xfce.mousepad.desktop
%{_datadir}/applications/org.xfce.mousepad-settings.desktop
%{_datadir}/glib-2.0/schemas/org.xfce.mousepad.gschema.xml
%{_datadir}/glib-2.0/schemas/org.xfce.mousepad.plugins.gspell.gschema.xml
%{_datadir}/icons/hicolor/*/*/*
%{_datadir}/polkit-1/actions/org.xfce.mousepad.policy

%changelog
* Sun Apr 03 2022 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.5.9

* Mon Nov 29 2021 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.5.8

* Thu Sep 23 2021 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.5.7

* Sun Sep 12 2021 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.5.6

* Thu May 13 2021 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.5.5

* Sat Apr 03 2021 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.5.4

* Sun Feb 28 2021 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.5.3

* Sat Jan 30 2021 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.5.2

* Sat Dec 26 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.5.1

* Fri Jun 08 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.4.1

* Sat Jul 22 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Add patches

* Fri Feb 10 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.4.0-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.4.0-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Wed Jun 17 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.4.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Thu Mar 26 2015 Richard Hughes <rhughes@redhat.com> - 0.4.0-3
- Add an AppData file for the software center

* Sun Mar 01 2015 Kevin Fenzi <kevin@scrye.com> 0.4.0-2
- Fix glib schemas

* Sat Feb 28 2015 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.4.0-1
- Update to 0.4.0

* Sun Aug 17 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.0-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Sat Aug 03 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Sun Dec 30 2012 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.0-1
- Update to 0.3.0 final
- Clean up spec file

* Mon Sep 03 2012 Kevin Fenzi <kevin@scrye.com> 0.3.0-0.1
- Update to pre-release git snapshot of 0.3.0

* Fri Jul 20 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.16-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Wed Apr 04 2012 Kevin Fenzi <kevin@scrye.com> - 0.2.16-7
- Rebuild for Xfce 4.10

* Fri Jan 13 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.16-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Tue Dec 06 2011 Adam Jackson <ajax@redhat.com> - 0.2.16-5
- Rebuild for new libpng

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.16-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Mon Nov 01 2010 Kevin Fenzi <kevin@tummy.com> - 0.2.16-3
- Add patch to fix find bug (#648560)

* Sat Jul 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.16-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Thu Feb 26 2009 Kevin Fenzi <kevin@tummy.com> - 0.2.16-1
- Update to 0.2.16

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.14-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Oct 27 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.2.14-1
- Update to 0.2.14
- BuildRequire intltool
- Drop category X-Fedora from desktop file

* Sun Feb 10 2008 Kevin Fenzi <kevin@tummy.com> - 0.2.13-2
- Rebuild for gcc43

* Sun Nov 18 2007 Kevin Fenzi <kevin@tummy.com> - 0.2.13-1
- Update to 0.2.13

* Mon Aug 27 2007 Kevin Fenzi <kevin@tummy.com> - 0.2.12-3
- Update License tag

* Mon May 14 2007 Kevin Fenzi <kevin@tummy.com> - 0.2.12-2
- Rebuild for ppc64

* Sun Jan 21 2007 Kevin Fenzi <kevin@tummy.com> - 0.2.12-1
- Update to 0.2.12

* Fri Nov 10 2006 Kevin Fenzi <kevin@tummy.com> - 0.2.10-1
- Update to 0.2.10

* Thu Oct  5 2006 Kevin Fenzi <kevin@tummy.com> - 0.2.8-2
- Fix typo in description 

* Sun Sep  3 2006 Kevin Fenzi <kevin@tummy.com> - 0.2.8-1
- Update to 0.2.8

* Thu Aug 31 2006 Kevin Fenzi <kevin@tummy.com> - 0.2.6-2
- Add update-desktop-database

* Sun Aug 27 2006 Kevin Fenzi <kevin@tummy.com> - 0.2.6-1
- Inital package for fedora extras

