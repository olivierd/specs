%global	_version %{version}

# Test with Python 3 (disable by default)
%bcond_with python3

# Override some Python 3 macros
%if %with python3
%global py3_build() %{expand:\\\
    CFLAGS="${CFLAGS:-${RPM_OPT_FLAGS}}" LDFLAGS="${LDFLAGS:-${RPM_LD_FLAGS}}"\\\
    HGPYTHON3=1 %{__python3} %{py_setup} %{?py_setup_args} build --executable="%{__python3} %{py3_shbang_opts}" %{?*}
}

%global py3_install() %{expand:\\\
    CFLAGS="${CFLAGS:-${RPM_OPT_FLAGS}}" LDFLAGS="${LDFLAGS:-${RPM_LD_FLAGS}}"\\\
    HGPYTHON3=1 %{__python3} %{py_setup} %{?py_setup_args} install -O1 --skip-build --root %{buildroot} %{?*}
}
%endif


Name:		mercurial
Version:	5.2.2
Release:	99%{?dist}
Summary:	Mercurial -- a distributed SCM

License:	GPLv2+
URL:		https://www.mercurial-scm.org/
Source0:	http://www.mercurial-scm.org/release/%{name}-%{_version}.tar.gz

%if %with python3
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
%else
BuildRequires:  python2-devel
BuildRequires:  python2-setuptools
%endif
BuildRequires:  gettext
Requires:       ca-certificates

%description
Mercurial is a fast, lightweight source control manager system
designed for efficient handling of very large distributed projects.

%prep
%setup -q -n %{name}-%{_version}

%if %with python3
%build
%py3_build

%install
%py3_install
%else
%build
%py2_build

%install
%py2_install
%endif

%{__mkdir_p} %{buildroot}%{_mandir}/man1
%{__mkdir_p} %{buildroot}%{_mandir}/man5
%{__cp} doc/hg.1 %{buildroot}%{_mandir}/man1
%{__cp} doc/hgignore.5 %{buildroot}%{_mandir}/man5
%{__cp} doc/hgrc.5 %{buildroot}%{_mandir}/man5

%if %with python3
%{__mv} %{buildroot}%{python3_sitearch}/mercurial/locale/ \
    %{buildroot}%{_datadir}/locale/
%{__rm} -Rf %{buildroot}%{python3_sitearch}/mercurial/locale/
%else
%{__mv} %{buildroot}%{python2_sitearch}/mercurial/locale/ \
    %{buildroot}%{_datadir}/locale/
%{__rm} -Rf %{buildroot}%{python2_sitearch}/mercurial/locale/
%endif

%find_lang hg

%{__mkdir_p} %{buildroot}%{_sysconfdir}/mercurial/hgrc.d

%{__cat} > certs.rc <<EOF
# see: http://mercurial.selenic.com/wiki/CACertificates
[web]
cacerts = /etc/pki/tls/certs/ca-bundle.crt
EOF
%{__install} -m 644 certs.rc %{buildroot}%{_sysconfdir}/mercurial/hgrc.d

%files -f hg.lang
%defattr(-,root,root,-)
%license COPYING
%doc %attr(644,root,root) %{_mandir}/man?/hg*.gz
%{_bindir}/hg
%dir %{_sysconfdir}/mercurial
%dir %{_sysconfdir}/mercurial/hgrc.d
%config(noreplace) %{_sysconfdir}/mercurial/hgrc.d/certs.rc
%if %with python3
%{python3_sitearch}/*
%else
%{python2_sitearch}/*
%endif


%changelog
* Mon Jan 06 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 5.2.2

* Fri Dec 06 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 5.2.1

* Tue Nov 05 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 5.2

* Sat Nov 02 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 5.2rc0

* Sat Oct 05 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 5.1.2

* Sat Sep 07 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 5.1.1

* Thu Aug 01 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 5.1

* Fri Jul 26 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 5.1rc0

* Sat Jul 13 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 5.0.2

* Wed Jun 05 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 5.0.1

* Sat May 04 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 5.0

* Fri Apr 19 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 5.0rc0

* Tue Mar 19 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.9.1

* Fri Feb 01 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.9

* Sat Jan 19 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.9rc0

* Tue Jan 08 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.8.2

* Wed Dec 05 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.8.1

* Sat Nov 03 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.8

* Thu Nov 01 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.8rc0

* Tue Oct 02 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.7.2

* Tue Sep 04 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.7.1

* Wed Aug 01 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.7

* Sat Jul 21 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.7rc0

* Wed Jul 04 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.6.2

* Thu Jun 07 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.6.1

* Sun May 06 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.6

* Sun Apr 22 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.6rc1

* Sun Apr 08 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.5.3

* Tue Mar 06 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.5.2

* Sun Feb 04 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.5

* Wed Jan 24 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.5-rc

* Sat Dec 02 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.4.2

* Thu Nov 09 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.4.1

* Sat Nov 04 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.4

* Mon Oct 23 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.4-rc

* Mon Oct 02 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.3.3

* Mon Sep 18 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.3.2

* Fri Aug 11 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.3.1

* Sat Jul 22 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.3-rc

* Wed Jul 05 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.2.2
- Replace some commands by their macros

* Sun Jun 04 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.2.1

* Fri May 05 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.2

* Sat Apr 22 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.2-rc
