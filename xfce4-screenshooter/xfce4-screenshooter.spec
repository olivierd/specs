%global major 1.9

Name:           xfce4-screenshooter
Version:        1.9.10
Release:        99%{?dist}
Summary:        Screenshot utility for the Xfce desktop

License:        GPLv2+
URL:            https://goodies.xfce.org/projects/applications/%{name}
Source0:        https://archive.xfce.org/src/apps/%{name}/%{major}/%{name}-%{version}.tar.bz2

BuildRequires:  libtool
BuildRequires:  intltool
BuildRequires:  gettext

BuildRequires:  pkgconfig(libxfce4panel-2.0) >= 4.15.0
BuildRequires:  pkgconfig(libxfce4util-1.0)
BuildRequires:  pkgconfig(libxfce4ui-2)
BuildRequires:  pkgconfig(gtk+-3.0) >= 3.22.0
BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(gthread-2.0)
BuildRequires:  pkgconfig(libsoup-2.4)
BuildRequires:  pkgconfig(libxml-2.0)
BuildRequires:  pkgconfig(exo-2) >= 4.16.0
BuildRequires:  pkgconfig(x11)
BuildRequires:  pkgconfig(xext)
BuildRequires:  pkgconfig(xfixes)
BuildRequires:  pkgconfig(pango)

BuildRequires:  desktop-file-utils
BuildRequires:  gtk-update-icon-cache
BuildRequires:  libappstream-glib

%description
The Xfce Screenshooter utility allows you to capture the entire screen, the 
active window or a selected region. You can set the delay that elapses before 
the screenshot is taken and the action that will be done with the screenshot: 
save it to a PNG file, copy it to the clipboard, or open it using another 
application.

%package        plugin
Summary:        Screenshot utility for the Xfce panel
Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       xfce4-panel-devel >= 4.15.0
Requires:       libxfce4ui-devel

%description    plugin
The Xfce Screenshooter plugin allows you to take screenshots from the Xfce 
panel.

%prep
%setup -q

%build
%configure --disable-static

%make_build

%install
%make_install

find %{buildroot} -name '*.la' | xargs %{__rm}

# make sure debuginfo is generated properly
%{__chmod} -c +x %{buildroot}%{_libdir}/xfce4/panel/plugins/*.so

%find_lang %{name}

%check
appstream-util validate-relax --nonet \
    %{buildroot}%{_datadir}/metainfo/xfce4-screenshooter.appdata.xml

%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog NEWS TODO
%license COPYING
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/*/*
%{_metainfodir}/xfce4-screenshooter.appdata.xml
%{_mandir}/man1/*.1.*

%files plugin
%defattr(-,root,root,-)
%{_libdir}/xfce4/panel/plugins/*.so
%{_datadir}/xfce4/panel/plugins/*.desktop

%changelog
* Mon Mar 07 2022 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.9.10

* Sun May 23 2021 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.9.9

* Sat Dec 26 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.9.8

* Sun Nov 03 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.9.7

* Sun Oct 20 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Fix build with xfce4-panel >= 4.15

* Mon Aug 26 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.9.6

* Sat Jun 22 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Rebuild for Fedora 30

* Sun Mar 31 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.9.5

* Sat Mar 09 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.9.4

* Wed Nov 07 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Sync with Fedora 29

* Sat Aug 11 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.9.3

* Tue May 01 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.9.2

* Fri Jul 14 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.9.1

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.8.2-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Fri Feb 05 2016 Fedora Release Engineering <releng@fedoraproject.org> - 1.8.2-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Fri Jun 19 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.8.2-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Thu Mar 05 2015 Kevin Fenzi <kevin@scrye.com> 1.8.2-4
- Rebuild again for Xfce 4.12

* Sat Feb 28 2015 Kevin Fenzi <kevin@scrye.com> 1.8.2-3
- Rebuild for Xfce 4.12

* Sat Feb 21 2015 Till Maas <opensource@till.name> - 1.8.2-2
- Rebuilt for Fedora 23 Change
  https://fedoraproject.org/wiki/Changes/Harden_all_packages_with_position-independent_code

* Sun Jan 25 2015 Kevin Fenzi <kevin@scrye.com> 1.8.2-1
- Update to 1.8.2

* Mon Aug 18 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.8.1-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Thu Jun 19 2014 Yaakov Selkowitz <yselkowi@redhat.com> - 1.8.1-4
- Fix FTBFS with automake-1.14 (#1107271)
- Cleanup spec

* Sun Jun 08 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.8.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Sun Aug 04 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.8.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Sun May 19 2013 Christoph Wickert <cwickert@fedoraproject.org> - 1.8.1-1
- Update to 1.8.1 (fixes #895968)
- Upstream build fixes: Build plugin as module, no versioned libs,
  only export needed symbols
- Add aarch64 support (#926787)
- Increase transparency of selection background (bugzilla.xfce.org #9592)

* Fri Feb 15 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.8.0-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Sun Jul 22 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.8.0-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Sun Apr 15 2012 Kevin Fenzi <kevin@scrye.com> - 1.8.0-6
- Rebuild for Xfce 4.10(pre2)

* Fri Apr 06 2012 Christoph Wickert <cwickert@fedoraproject.org> - 1.8.0-5
- Update manpage (#809491)

* Thu Apr 05 2012 Kevin Fenzi <kevin@scrye.com> - 1.8.0-4
- Rebuild for Xfce 4.10

* Tue Feb 28 2012 Christoph Wickert <cwickert@fedoraproject.org> - 1.8.0-3
- Bring back the dsofix patch
- Rebuild for new libpng

* Wed Aug 03 2011 Christoph Wickert <cwickert@fedoraproject.org> - 1.8.0-2
- Drop dsofix patch, no longer needed

* Mon Aug 01 2011 Christoph Wickert <cwickert@fedoraproject.org> - 1.8.0-1
- Update to 1.8.0
- No longer require xfce4-doc but own %%{_datadir}/xfce4/doc/ (#721291)

* Mon Feb 07 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.7.9-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sun Dec 19 2010 Christoph Wickert <cwickert@fedoraproject.org> - 1.7.9-3
- Rebuild for xfce4-panel 4.7

* Tue May 18 2010 Christoph Wickert <cwickert@fedoraproject.org> - 1.7.9-2
- Add patch to fix DSO linking (#564819)

* Sun Feb 07 2010 Christoph Wickert <cwickert@fedoraproject.org> - 1.7.9-1
- Update to 1.7.9 (RC for 1.8.0)
- Include NEWS and TODO

* Thu Jul 30 2009 Christoph Wickert <cwickert@fedoraproject.org> - 1.6.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Sun Jun 14 2009 Christoph Wickert <cwickert@fedoraproject.org> - 1.6.0-1
- Update to 1.6.0

* Wed Feb 25 2009 Christoph Wickert <cwickert@fedoraproject.org> - 1.5.1-1
- Update to 1.5.1
- Built for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sun Jan 18 2009 Christoph Wickert <cwickert@fedoraproject.org> - 1.5.0-1
- Update to 1.5.0 on Xfce 4.5.93.

* Fri Jan 02 2009 Christoph Wickert <cwickert@fedoraproject.org> - 1.4.90.0-1
- Update to 1.4.90.0
- Split package into standalone app and panel plugin

* Thu Nov 27 2008 Christoph Wickert <cwickert@fedoraproject.org> - 1.4.0-1
- Update to 1.4.0

* Wed Aug 27 2008 Christoph Wickert <cwickert@fedoraproject.org> - 1.3.2-1
- Update to 1.3.2

* Wed Aug 27 2008 Christoph Wickert <cwickert@fedoraproject.org> - 1.3.2-1
- Update to 1.3.2

* Fri Jul 18 2008 Christoph Wickert <cwickert@fedoraproject.org> - 1.3.1-1
- Update to 1.3.1

* Wed Jul 16 2008 Christoph Wickert <cwickert@fedoraproject.org> - 1.3.0-1
- Update to 1.3.0

* Thu Jul 03 2008 Christoph Wickert <cwickert@fedoraproject.org> - 1.2.0-1
- Update to 1.2.0
- Include new xfce4-screenshooter manpage

* Sat Jun 21 2008 Christoph Wickert <cwickert@fedoraproject.org> - 1.1.0-1
- Update to 1.1.0
- BR gettext

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 1.0.0-7
- Autorebuild for GCC 4.3

* Sat Aug 25 2007 Christoph Wickert <cwickert@fedoraproject.org> - 1.0.0-6
- Change license tag to GPLv2+

* Sat Apr 28 2007 Christoph Wickert <cwickert@fedoraproject.org> - 1.0.0-5
- Rebuild for Xfce 4.4.1

* Sun Jan 28 2007 Christoph Wickert <cwickert@fedoraproject.org> - 1.0.0-4
- Rebuild for XFCE 4.4.

* Thu Oct 05 2006 Christoph Wickert <cwickert@fedoraproject.org> - 1.0.0-3
- Bump release for devel checkin.

* Wed Sep 13 2006 Christoph Wickert <cwickert@fedoraproject.org> - 1.0.0-2
- Rebuild for XFCE 4.3.99.1.
- BR perl(XML::Parser).

* Mon Sep 04 2006 Christoph Wickert <cwickert@fedoraproject.org> - 1.0.0-1
- Update to 1.0.0 on XFCE 4.3.90.2.

* Mon Sep 04 2006 Christoph Wickert <cwickert@fedoraproject.org> - 0.0.8-4
- Mass rebuild for Fedora Core 6.

* Tue Apr 11 2006 Christoph Wickert <fedora.wickert@arcor.de> - 0.0.8-3
- Require xfce4-panel.

* Thu Feb 16 2006 Christoph Wickert <fedora.wickert@arcor.de> - 0.0.8-2
- Rebuild for Fedora Extras 5.

* Sat Jan 21 2006 Christoph Wickert <fedora.wickert@arcor.de> - 0.0.8-1
- Initial Fedora Extras version.
