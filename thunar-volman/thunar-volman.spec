%global major 4.16

Name:           thunar-volman
Version:        4.16.0
Release:        99%{?dist}
Summary:        Automatic management of removable drives and media for Thunar

License:        GPLv2+
URL:            https://goodies.xfce.org/projects/thunar-plugins/%{name}
Source0:        https://archive.xfce.org/src/xfce/%{name}/%{major}/%{name}-%{version}.tar.bz2

BuildRequires:	libtool
BuildRequires:	intltool
BuildRequires:	gettext

BuildRequires:	pkgconfig(exo-2)
BuildRequires:	pkgconfig(glib-2.0)
BuildRequires:	pkgconfig(gio-2.0)
BuildRequires:	pkgconfig(gthread-2.0)
BuildRequires:	pkgconfig(gtk+-3.0) >= 3.20.0
BuildRequires:	pkgconfig(gudev-1.0)
BuildRequires:	pkgconfig(libxfce4ui-2) >= 4.15.0
BuildRequires:	pkgconfig(libxfce4util-1.0)
BuildRequires:	pkgconfig(libxfconf-0)
BuildRequires:	pkgconfig(libnotify)

BuildRequires:  desktop-file-utils
BuildRequires:	gtk-update-icon-cache

Requires:	libxfce4ui >= 4.15.0
Requires:	exo
Requires:	thunar >= 4.15.0

%description
The Thunar Volume Manager is an extension for the Thunar file manager, which 
enables automatic management of removable drives and media. For example, if 
thunar-volman is installed and configured properly, and you plug in your 
digital camera, it will automatically launch your preferred photo application 
and import the new pictures from the camera into your photo collection.


%prep
%setup -q

%build
%configure --disable-static

%make_build

%install
%make_install

%find_lang %{name}

%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
if [ "$1" -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog NEWS THANKS
%license COPYING
%{_bindir}/thunar-volman
%{_bindir}/thunar-volman-settings
%{_datadir}/icons/*/*/*/*
%{_datadir}/applications/*thunar-volman-settings.desktop


%changelog
* Sat Dec 26 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.16.0

* Mon Nov 02 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.15.1

* Sat Aug 22 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.15.0

* Mon Aug 12 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.9.5

* Sun Jul 28 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.9.4

* Sun Jun 30 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.9.3

* Sat Jun 22 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Rebuild for Fedora 30

* Sat May 18 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.9.2

* Tue Nov 27 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.9.1

* Wed Nov 07 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Sync with Fedora 29

* Wed Jan 03 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.9.0

* Sat Aug 19 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Rebuilt

* Thu Aug 03 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.8.1-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.8.1-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.8.1-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Fri Feb 05 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.8.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Fri Jun 19 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Tue Mar 17 2015 Kevin Fenzi <kevin@scrye.com> 0.8.1-1
- Update to 0.8.1

* Sat Feb 28 2015 Kevin Fenzi <kevin@scrye.com> 0.8.0-9
- Rebuild for Xfce 4.12

* Sat Feb 21 2015 Till Maas <opensource@till.name> - 0.8.0-8
- Rebuilt for Fedora 23 Change
  https://fedoraproject.org/wiki/Changes/Harden_all_packages_with_position-independent_code

* Mon Aug 18 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.0-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Sun Jun 08 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.0-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Sun Aug 04 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.0-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Fri Feb 22 2013 Toshio Kuratomi <toshio@fedoraproject.org> - 0.8.0-4
- Remove --vendor from desktop-file-install https://fedorahosted.org/fesco/ticket/1077

* Fri Feb 15 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Sat Jul 21 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Sun Apr 29 2012 Christoph Wickert <cwickert@fedoraproject.org> - 0.8.0-1
- Update to 0.8.0 (Xfce 4.10 final)
- Add VCS key and review #

* Sat Apr 14 2012 Kevin Fenzi <kevin@scrye.com> - 0.7.1-1
- Update to 0.7.1 (Xfce 4.10pre2)

* Tue Apr 03 2012 Kevin Fenzi <kevin@scrye.com> - 0.7.0-1
- Update to 0.7.0

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.6.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Wed Feb 09 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.6.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sun Jan 16 2011 Kevin Fenzi <kevin@tummy.com> - 0.6.0-1
- Update to 0.6.0 (for Thunar 1.2.0 on Xfce 4.8 final)

* Sun Dec 05 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.5.3-1
- Update to 0.5.3 (for Thunar 1.1.5 on Xfce 4.8 pre2)

* Sat Nov 06 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.5.2-1
- Update to 0.5.2 (for Thunar 1.1.4 on Xfce 4.8 pre1)
- BR libgudev1-devel
- Fix for libnotify 0.7.0 (bugzilla.xfce.org #6916)

* Sat Nov 06 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.80-5
- Fix missing icons (#650504)

* Sun Oct 03 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.80-4
- Fix the parole patch (#639484)
- Update gtk-icon-cache scriptlets

* Sat Oct 02 2010 Kevin Fenzi <kevin@tummy.om> - 0.3.80-3
- Add patch to default to parole for video/audio (#639484)

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.80-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Sat Feb 28 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.80-1
- Update to 0.3.80
- Add Category X-XfceSettingsDialog

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.2.0-2
- Autorebuild for GCC 4.3

* Mon Dec 03 2007 Christoph Wickert <cwickert@fedoraproject.org> - 0.2.0-1
- Update to 0.2.0 and Thunar 0.9.0

* Tue Aug 21 2007 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.2-2
- Rebuild for BuildID feature

* Sun Jan 21 2007 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.2-1
- Update to 0.1.2.

* Wed Jan 17 2007 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.0-1
- Initial packaging.
