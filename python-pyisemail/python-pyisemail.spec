%global srcname pyIsEmail
%global modname pyisemail

Name:		python-%{modname}
Version:	1.3.2
Release:	1%{?dist}
Summary:	Simple, robust email validation in Python

License:	MIT
URL:		http://michaelherold.github.io/pyIsEmail/
Source0:    https://github.com/michaelherold/%{srcname}/archive/v%{version}.tar.gz

BuildArch:  noarch

%description
pyIsEmail is a no-nonsense approach for checking whether that
user-supplied email address could be real. It allows you to validate,
and even check the domain.
When you want to know why an email address does not validate, we even
provide you with a diagnosis.

%package -n python2-%{modname}
Summary:        %{summary}
%{?python_provide:%python_provide python2-%{modname}}
BuildRequires:	python2-devel
BuildRequires:	python2-setuptools
BuildRequires:	python2-dns
Requires:       python2-dns

%description -n python2-%{modname}
pyIsEmail is a no-nonsense approach for checking whether that
user-supplied email address could be real. It allows you to validate;
and even check the domain.
when you want to know why an email address does not validate, we even
provide you with a diagnosis.

Python 2 version.

%package -n python3-%{modname}
Summary:       %{summary}
%{?python_provide:%python_provide python3-%{modname}}
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-dns
Requires:       python3-dns

%description -n python3-%{modname}
pyIsEmail is a no-nonsense approach for checking whether that
user-supplied email address could be real. It allows you to validate;
and even check the domain.
when you want to know why an email address does not validate, we even
provide you with a diagnosis.

Python 3 version.

%prep
%setup -q -n %{srcname}-%{version}

%build
%py2_build
%py3_build

%install
%py2_install
%py3_install

%files -n python2-%{modname}
%license LICENSE
%doc README.rst
%{python2_sitelib}/*

%files -n python3-%{modname}
%license LICENSE
%doc README.rst
%{python3_sitelib}/*

%changelog
* Sun Oct 07 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Initial package (1.3.2)

