%global major 4.16

Name:           xfconf
Version:        4.16.0
Release:        99%{?dist}
Summary:        Hierarchical configuration system for Xfce

License:        GPLv2
URL:            https://www.xfce.org/
Source0:        https://archive.xfce.org/src/xfce/%{name}/%{major}/%{name}-%{version}.tar.bz2

BuildRequires:	gettext
BuildRequires:	intltool
BuildRequires:	perl

BuildRequires:	pkgconfig(x11)
BuildRequires:	pkgconfig(gobject-2.0) >= 2.42.0
BuildRequires:	pkgconfig(gio-2.0) >= 2.42.0
BuildRequires:	pkgconfig(gio-unix-2.0) >= 2.42.0
BuildRequires:	pkgconfig(gthread-2.0) >= 2.42.0
BuildRequires:	pkgconfig(libxfce4util-1.0) >= 4.14.0
BuildRequires:  pkgconfig(gobject-introspection-1.0)
BuildRequires:  vala

%description
Xfconf is a hierarchical (tree-like) configuration system where the
immediate child nodes of the root are called "channels".  All settings
beneath the channel nodes are called "properties."

%package        devel
Summary:        Development tools for xfconf
Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       glib2-devel
Requires:       gobject-introspection-devel
Requires:       vala

%description devel
This package contains libraries and header files for the
%{name} library.

%prep
%setup -q

%build
%configure --disable-static \
	--enable-introspection=yes \
	--enable-vala \
	--without-html-dir

%{__sed} -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
%{__sed} -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

export LD_LIBRARY_PATH=$(pwd)/xfconf/.libs:${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}

%make_build

%install
%make_install

# fix permissions for installed libraries
%{__chmod} 755 %{buildroot}/%{_libdir}/*.so
 
find %{buildroot} -type f -name '*.la' | xargs %{__rm}

%find_lang %{name}

%ldconfig_scriptlets devel

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog NEWS TODO
%license COPYING
%{_libdir}/libxfconf-0.so.*
%{_libdir}/gio/modules/libxfconfgsettingsbackend.so
%{_bindir}/xfconf-query
%dir %{_libdir}/xfce4/xfconf
%{_libdir}/xfce4/xfconf/xfconfd
%{_datadir}/dbus-1/services/org.xfce.Xfconf.service
%dir %{_datadir}/bash-completion
%dir %{_datadir}/bash-completion/completions
%{_datadir}/bash-completion/completions/xfconf-query

%files devel
%defattr(-,root,root,-)
%{_libdir}/libxfconf-0.so
%{_libdir}/girepository-1.0/Xfconf-0.typelib
%{_libdir}/pkgconfig/libxfconf-0.pc
%{_includedir}/xfce4/xfconf-0/*
%{_datadir}/gir-1.0/Xfconf-0.gir
%{_datadir}/vala/vapi/libxfconf-0.*


%changelog
* Sat Dec 26 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.16.0

* Mon Nov 09 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.15.1

* Thu Jul 16 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.15.0

* Wed May 06 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.14.3

* Mon Sep 09 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Improve GObject Introspection support (avoid warning)

* Mon Aug 12 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.14.1

* Thu Jun 27 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.13.8

* Sat Jun 22 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Rebuild for Fedora 30

* Sat May 18 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.13.7

* Fri Feb 15 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Improve GObject introspection support

* Sun Feb 03 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Test new patches for GObject Introspection support

* Sat Jan 05 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Add GObject Introspection

* Tue Nov 06 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Sync with Fedora 29

* Mon Oct 22 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.13.6

* Mon Jun 18 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.13.5

* Sat Jun 09 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Add upstream patch (Initialize GValue for empty arrays)

* Thu Aug 31 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.13.4

* Mon Jul 24 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.13.3

* Sun Jul 09 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.13.2

* Tue Jun 06 2017 Jitka Plesnikova <jplesnik@redhat.com> - 4.12.1-3
- Perl 5.26 rebuild

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 4.12.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Mon Oct 24 2016 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 4.12.1-1
- Update to 4.12.1

* Mon May 16 2016 Jitka Plesnikova <jplesnik@redhat.com> - 4.12.0-5
- Perl 5.24 rebuild

* Fri Feb 05 2016 Fedora Release Engineering <releng@fedoraproject.org> - 4.12.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Fri Jun 19 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.12.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Mon Jun 08 2015 Jitka Plesnikova <jplesnik@redhat.com> - 4.12.0-2
- Perl 5.22 rebuild

* Sat Feb 28 2015 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 4.12.0-1
- Update to stable release 4.12.0
- Fix permissions for installed libraries

* Sat Feb 21 2015 Till Maas <opensource@till.name> - 4.10.0-10
- Rebuilt for Fedora 23 Change
  https://fedoraproject.org/wiki/Changes/Harden_all_packages_with_position-independent_code

* Mon Sep 01 2014 Jitka Plesnikova <jplesnik@redhat.com> - 4.10.0-9
- Perl 5.20 rebuild

* Mon Aug 18 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.10.0-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Sun Jun 08 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.10.0-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Sun Aug 04 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.10.0-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Thu Jul 18 2013 Petr Pisar <ppisar@redhat.com> - 4.10.0-5
- Perl 5.18 rebuild

* Fri Feb 15 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.10.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Sun Jul 22 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.10.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Tue Jun 12 2012 Petr Pisar <ppisar@redhat.com> - 4.10.0-2
- Perl 5.16 rebuild

* Sat Apr 28 2012 Christoph Wickert <cwickert@fedoraproject.org> - 4.10.0-1
- Update to 4.10.0 final
- Make build verbose
- Add VCS key

* Sat Apr 14 2012 Kevin Fenzi <kevin@scrye.com> - 4.9.1-1
- Update to 4.9.1 (Xfce 4.10pre2)

* Sun Apr 01 2012 Kevin Fenzi <kevin@scrye.com> - 4.9.0-1
- Update to 4.9.0

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.8.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Fri Dec 23 2011 Kevin Fenzi <kevin@scrye.com> - 4.8.1-1
- Update to 4.8.1

* Thu Jul 21 2011 Petr Sabata <contyk@redhat.com> - 4.8.0-4
- Perl mass rebuild

* Mon Jun 20 2011 Marcela Mašláňová <mmaslano@redhat.com> - 4.8.0-3
- Perl mass rebuild

* Mon Feb 07 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.8.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sun Jan 16 2011 Kevin Fenzi <kevin@tummy.com> - 4.8.0-1
- Update to 4.8.0 final. 

* Sun Jan 02 2011 Christoph Wickert <cwickert@fedoraproject.org> - 4.7.5-1
- Update to 4.7.5

* Fri Dec 03 2010 Christoph Wickert <cwickert@fedoraproject.org> - 4.7.4-1
- Update to 4.7.4
- Fix directory ownership

* Sun Sep 05 2010 Christoph Wickert <cwickert@fedoraproject.org> - 4.7.3-1
- Update to 4.7.3

* Mon Aug 23 2010 Kevin Fenzi <kevin@tummy.com> - 4.6.2-3
- Remove unneeded gtk-doc dep. Fixes bug #604423

* Wed Jun 02 2010 Marcela Maslanova <mmaslano@redhat.com> - 4.6.2-2
- Mass rebuild with perl-5.12.0

* Fri May 21 2010 Kevin Fenzi <kevin@tummy.com> - 4.6.2-1
- Update to 4.6.2

* Mon Dec  7 2009 Stepan Kasal <skasal@redhat.com> - 4.6.1-5
- rebuild against perl 5.10.1

* Tue Oct 20 2009 Orion Poplawski <orion@cora.nwra.com> - 4.6.1-4
- Add BR perl(ExtUtils::MakeMaker) and perl(Glib::MakeHelper)

* Mon Jul 27 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.6.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Sun Jun 14 2009 Christoph Wickert <cwickert@fedoraproject.org> - 4.6.1-2
- Require dbus-x11 (#505499)

* Sun Apr 19 2009 Kevin Fenzi <kevin@tummy.com> - 4.6.1-1
- Update to 4.6.1

* Mon Mar 02 2009 Christoph Wickert <cwickert@fedoraproject.org> - 4.6.0-2
- Fix directory ownership problems
- Move gtk-doc into devel package and mark it %%doc
- Make devel package require gtk-doc

* Thu Feb 26 2009 Kevin Fenzi <kevin@tummy.com> - 4.6.0-1
- Update to 4.6.0

* Thu Feb 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.5.99.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Jan 26 2009 Kevin Fenzi <kevin@tummy.com> - 4.5.99.1-1
- Update to 4.5.99.1

* Thu Jan 22 2009 Christoph Wickert <cwickert@fedoraproject.org> - 4.5.93-3
- Let xfce4-settings Obsolete mcs manager and plugin packages

* Thu Jan 22 2009 Christoph Wickert <cwickert@fedoraproject.org> - 4.5.93-2
- Add Obsoletes for mcs devel package

* Tue Jan 13 2009 Kevin Fenzi <kevin@tummy.com> - 4.5.93-1
- Update to 4.5.93

* Fri Jan 02 2009 Kevin Fenzi <kevin@tummy.com> - 4.5.92-4
- Add Obsoletes for mcs packages

* Mon Dec 22 2008 Kevin Fenzi <kevin@tummy.com> - 4.5.92-3
- Fixes for review ( bug 477732 )

* Mon Dec 22 2008 Kevin Fenzi <kevin@tummy.com> - 4.5.92-2
- Add gettext BuildRequires

* Sun Dec 21 2008 Kevin Fenzi <kevin@tummy.com> - 4.5.92-1
- Initial version for Fedora
