Name:		pkg
Version:	1.10.5
Release:	1%{?dist}
Summary:	package management tool for FreeBSD

License:	BSD
URL:		https://github.com/freebsd/pkg
Source0:	%{url}/archive/%{version}.tar.gz
Patch0:     Fix-mandir.patch
Patch1:     Remove-periodic-files.patch

BuildRequires:	autoconf
BuildRequires:	automake
BuildRequires:	pkgconf
BuildRequires:	libtool
BuildRequires:	perl

BuildRequires:	pkgconfig(libarchive)
BuildRequires:	pkgconfig(openssl)
BuildRequires:	pkgconfig(libbsd)
BuildRequires:	pkgconfig(ldns)

%description
pkg is a binary package manager for FreeBSD

%prep
%setup -q -n %{name}-%{version}

# fix build on Linux
%{__sed} -i 's!readelf!LC_ALL=C readelf!' \
	external/libelf/native-elf-format

# fix pkgconfig/ path
%{__sed} -i 's!libdata!share!' \
	Makefile.am

%patch0 -p0
%patch1 -p0

NOCONFIGURE=1 ./autogen.sh

%build
%configure \
	--with-gnu-ld

%install
%make_install

find %{buildroot} -name '*.a' | xargs %{__rm}
find %{buildroot} -name '*.la' | xargs %{__rm}

%{__mkdir_p} %{buildroot}%{_sysconfdir}/pkg

%{__cat} > pkg.conf <<EOF
REPOS_DIR [
    "/etc/pkg/",
]
ABI = "freebsd:12:x86:64";
PKG_PLUGINS_DIR = "/usr/lib/pkg/";
PLUGINS_CONF_DIR = "/etc/pkg/";
EOF
%{__install} -m 644 pkg.conf %{buildroot}%{_sysconfdir}

%files
%license COPYING
%doc AUTHORS FAQ.md NEWS README.md TODO.md
%{_includedir}/pkg.h
%{_libdir}/libpkg*
%{_sbindir}/pkg
%{_datadir}/zsh/*/*
%{_datadir}/pkgconfig/*.pc
%{_mandir}/*/*
%{_sysconfdir}/bash_completion.d/_pkg.bash
%dir %{_sysconfdir}/pkg
%config(noreplace) %{_sysconfdir}/pkg.conf.sample
%config(noreplace) %{_sysconfdir}/pkg.conf

%changelog
* Sun Dec 09 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- 1.10.5 release
