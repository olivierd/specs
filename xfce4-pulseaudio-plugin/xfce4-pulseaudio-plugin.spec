%global major 0.4

Name:		xfce4-pulseaudio-plugin
Version:	0.4.3
Release:	99%{?dist}
Summary:	Pulseaudio plugin for Xfce4

License:	GPLv2+
URL:		https://goodies.xfce.org/projects/panel-plugins/%{name}
Source0:	https://archive.xfce.org/src/panel-plugins/%{name}/%{major}/%{name}-%{version}.tar.bz2

BuildRequires:	libtool
BuildRequires:	intltool
BuildRequires:	gettext

BuildRequires:	pkgconfig(x11)
BuildRequires:	pkgconfig(libpulse-mainloop-glib)
BuildRequires:	pkgconfig(glib-2.0) >= 2.42.0
BuildRequires:	pkgconfig(gio-2.0) >= 2.42.0
BuildRequires:	pkgconfig(gtk+-3.0) >= 3.20.0
BuildRequires:	pkgconfig(exo-2)
BuildRequires:	pkgconfig(libxfce4util-1.0)
BuildRequires:	pkgconfig(libxfce4ui-2) >= 4.13.0
BuildRequires:	pkgconfig(libxfce4panel-2.0) >= 4.13.0
BuildRequires:	pkgconfig(libxfconf-0)
BuildRequires:	pkgconfig(keybinder-3.0)
BuildRequires:	pkgconfig(libnotify)
BuildRequires:	pkgconfig(libwnck-3.0) >= 3.20.0
BuildRequires:	gtk-update-icon-cache

Requires:	pulseaudio
Requires:	pavucontrol
Requires:	xfce4-panel >= 4.13.0
Requires:	libxfce4ui >= 4.13.0

Obsoletes:	xfce4-mixer <= 4.11

%description
Pulseaudio panel plugin for Xfce Desktop Environment

%prep
%setup -q

%build
%configure --disable-static \
	--enable-mpris2 \
	--enable-wnck \
	--with-mixer-command=%{_bindir}/pavucontrol
%make_build

%install
%make_install

# remove libtool archives
find %{buildroot} -name '*.la' | xargs %{__rm}

%find_lang %{name}

%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog INSTALL NEWS README THANKS
%license COPYING
%{_datadir}/xfce4/panel/plugins/pulseaudio.desktop
%{_libdir}/xfce4/panel/plugins/libpulseaudio-plugin.so
%{_datadir}/icons/hicolor/*/*/*

%changelog
* Sun Mar 29 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.4.3

* Sun Aug 11 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.4.2

* Wed Nov 07 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Sync with Fedora 29

* Thu Apr 12 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.4.1

* Tue Mar 20 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.4.0

* Tue Feb 27 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.3.5

* Sat Feb 24 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Apply upstream patch

* Thu Dec 07 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.3.4

* Sat Nov 25 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.3.3

* Sat Nov 18 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Apply 2 patches

* Sun Oct 29 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.3.2

* Fri Sep 29 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.3.1

* Wed Sep 06 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.3.0

* Sat Aug 05 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Rebuilt
- Cleanup .spec

* Thu Aug 03 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.2.5-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.2.5-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Sat May 20 2017 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.2.5-2
- Add pavucontrol and pulseaudio as requires

* Sat Apr 22 2017 Filipe Rosset <rosset.filipe@gmail.com> - 0.2.5-1
- Rebuilt for new upstream 0.2.5 release

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.2.4-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Sat Sep 10 2016 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.2.4-5
- Removed obsoletes for xfce4-volumed and xfce4-mixer
- Fixes bug#1355763

* Sat Apr 16 2016 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.2.4-4
- Added obsoletes for xfce4-volumed

* Fri Apr 15 2016 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.2.4-3
- Added xfce4-mixer obsoletes

* Fri Feb 05 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.2.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Mon Oct 26 2015 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.2.4-1
- Update 0.2.4

* Fri Jun 19 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Mon May 11 2015 Kevin Fenzi <kevin@scrye.com> 0.2.3-1
- Update to 0.2.3

* Wed Apr 22 2015 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.2.2-2
- Add keybinder-devel as BR
- enable keyboard shortcuts support

* Thu Mar 26 2015 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.2.2-1
- Update to 0.2.2
- Added symoblic icons for different statuses

* Sun Mar 08 2015 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.2.1-1
- Update to 0.2.1
- Bug fix update

* Fri Mar 06 2015 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.2.0-1
- Update to 0.2.0

* Sun Mar 01 2015 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.1.0-1
- New package 
