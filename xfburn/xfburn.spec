%global major 0.6

Name:           xfburn
Version:        0.6.2
Release:        99%{?dist}
Summary:        Simple CD burning tool for Xfce

License:        GPLv2+
URL:            https://goodies.xfce.org/projects/applications/%{name}
VCS:            git://git.xfce.org/apps/xfburn
Source0:        https://archive.xfce.org/src/apps/%{name}/%{major}/%{name}-%{version}.tar.bz2

BuildRequires:	libtool
BuildRequires:	intltool
BuildRequires:	gettext

BuildRequires:	pkgconfig(libburn-1)
BuildRequires:	pkgconfig(libisofs-1)
BuildRequires:	pkgconfig(glib-2.0)
BuildRequires:	pkgconfig(gthread-2.0)
BuildRequires:	pkgconfig(gio-2.0)
BuildRequires:	pkgconfig(gio-unix-2.0)
BuildRequires:	pkgconfig(gtk+-3.0) >= 3.20
BuildRequires:	pkgconfig(libxfce4ui-2) >= 4.13.0
BuildRequires:	pkgconfig(exo-2)
BuildRequires:	pkgconfig(gudev-1.0) >= 145
BuildRequires:	pkgconfig(gstreamer-1.0)
BuildRequires:	pkgconfig(gstreamer-pbutils-1.0)

BuildRequires:  desktop-file-utils
BuildRequires:	gtk-update-icon-cache

Requires:       hicolor-icon-theme
Requires:       libxfce4ui >= 4.13.0

%description
Xfburn is a simple CD/DVD burning tool based on libburnia libraries. It can 
blank CD-RWs, burn and create iso images, as well as burn personal 
compositions of data to either CD or DVD.

%prep
%setup -q

# https://bugzilla.redhat.com/show_bug.cgi?id=1284977
%{__sed} -i 's!/tmp!/var/tmp!g' xfburn/xfburn-copy-cd-dialog.c
%{__sed} -i 's!/tmp!/var/tmp!g' xfburn/xfburn-copy-dvd-dialog.c
%{__sed} -i 's!/tmp!/var/tmp!' xfburn/xfburn-preferences-dialog.c

%build
%configure --disable-static --enable-gstreamer
%make_build

%install
%make_install

%find_lang %{name}

%post
update-desktop-database &> /dev/null ||:
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
update-desktop-database &> /dev/null ||:
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    %{_bindir}/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
%{_bindir}/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog NEWS TODO
%license COPYING
%{_bindir}/xfburn
%{_datadir}/applications/xfburn.desktop
%{_metainfodir}/org.xfce.xfburn.appdata.xml
%{_datadir}/Thunar/sendto/thunar-sendto-xfburn.desktop
%{_datadir}/icons/hicolor/*/*/*
%{_datadir}/xfburn/xfburn-popup-menus.ui
%{_datadir}/xfburn/xfburn-toolbars.ui
%{_datadir}/xfburn/xfburn.ui
%{_mandir}/man1/xfburn.1.gz


%changelog
* Sun Mar 08 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.6.2

* Sun Nov 03 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.6.1

* Thu Dec 14 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.5.5

* Sat Jul 22 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update patch (for icons)

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.5.4-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Thu Mar 24 2016 Richard Shaw <hobbes1069@gmail.com> - 0.5.4-5
- Change temp directory from /tmp to /var/tmp as images can get quite big.
- Modernize spec and bring up to current guidelines.

* Fri Feb 05 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.5.4-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Wed Sep 30 2015 Kevin Fenzi <kevin@scrye.com> 0.5.4-3
- Fix icon. Fixes bug #1265310

* Fri Jun 19 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Sun May 17 2015 Kevin Fenzi <kevin@scrye.com> 0.5.4-1
- Update to 0.5.4 and switch to gstreamer1

* Sat Feb 28 2015 Kevin Fenzi <kevin@scrye.com> 0.5.2-5
- Rebuild for Xfce 4.10

* Sat Feb 21 2015 Till Maas <opensource@till.name> - 0.5.2-4
- Rebuilt for Fedora 23 Change
  https://fedoraproject.org/wiki/Changes/Harden_all_packages_with_position-independent_code

* Mon Aug 18 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5.2-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Sun Jun 08 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Wed Apr 09 2014 Kevin Fenzi <kevin@scrye.com> 0.5.2-1
- Update to 0.5.2

* Thu Feb 20 2014 Kevin Fenzi <kevin@scrye.com> 0.5.0-1
- Update to 0.5.0

* Tue Nov 12 2013 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.4.3-13
- support for aarch64, patch from https://bugzilla.redhat.com/show_bug.cgi?id=926769
- modified bogus dates in changelog

* Sun Aug 04 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.4.3-12
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Fri Feb 15 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.4.3-11
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Fri Sep 28 2012 Christoph Wickert <cwickert@fedoraproject.org> - 0.4.3-10
- Fix crash when creating directory (#639804, #676086 and #851900)
- Fix crash when adding lots of files (#669971)
- Make sure desktop file validates

* Sun Jul 22 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.4.3-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Tue May 01 2012 Christoph Wickert <cwickert@fedoraproject.org> - 0.4.3-8
- Fix build with latest glib
- Add VCS key and review #

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.4.3-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Tue Dec 06 2011 Adam Jackson <ajax@redhat.com> - 0.4.3-6
- Rebuild for new libpng

* Tue Apr 26 2011 Christoph Wickert <cwickert@fedoraproject.org> - 0.4.3-5
- No longer BuildRequire obsolete hal (#699692)

* Mon Feb 07 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.4.3-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sat Dec 04 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.4.3-3
- Switch from Thunar-devel to thunar-vfs-devel

* Thu Apr 22 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.4.3-2
- Rebuild for libburn 0.8.0

* Sun Feb 14 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.4.3-1
- Update to 0.4.3

* Fri Jan 29 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.4.2-3
- Properly unmount drive before burning (#525514)

* Thu Oct 29 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.4.2-2
- Fix infinite loop in blank disk dialog (#525515)
- Don't crash on burning ISO image (#525518)

* Fri Jul 24 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.4.2-1
- Update to 0.4.2

* Wed Feb 25 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.4.1-1
- Update to 0.4.1
- Include new manpage

* Mon Jan 26 2009 Denis Leroy <denis@poolshark.org> - 0.4.0-1
- Update to upstream 0.4.0

* Tue Nov 04 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.91-2
- Don't enable debug
- Require hicolor-icon-theme

* Tue Nov 04 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.91-1
- Update to 0.3.91

* Wed Jul 16 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.2-1
- Update to 0.3.2

* Fri Jul 11 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.1-1
- Update to 0.3.1

* Wed Jun 11 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.0-1
- Update to 0.3.0 stable

* Mon Jan 07 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.0-0.1.20080107svn26552
- Update to 0.3.0svn-26552.
. Switch to libburn and drop requirements for genisoimage, cdrdao and wodim

* Sun Feb 25 2007 Christoph Wickert <cwickert@fedoraproject.org> - 0.1-0.1.20070225svn25032
- Initial package.
