%global major 4.16

Name:		xfce4-screensaver
Version:	4.16.0
Release:	99%{?dist}
Summary:	Xfce screensaver and locker

License:	GPLv2+ and LGPLv2+
Source0:	https://archive.xfce.org/src/apps/%{name}/%{major}/%{name}-%{version}.tar.bz2

BuildRequires:  intltool
BuildRequires:  libtool
BuildRequires:  gettext

BuildRequires:  pkgconfig(xrandr)
BuildRequires:  pkgconfig(x11)
BuildRequires:  pkgconfig(xscrnsaver)
BuildRequires:  pkgconfig(xext)
BuildRequires:  pkgconfig(xxf86vm)
BuildRequires:  pkgconfig(libxklavier)
BuildRequires:  pkgconfig(gtk+-3.0)
BuildRequires:  pkgconfig(dbus-glib-1)
BuildRequires:  pkgconfig(gio-2.0)
BuildRequires:  pkgconfig(gthread-2.0)
BuildRequires:  pkgconfig(gobject-2.0)
BuildRequires:  pkgconfig(libxfconf-0) >= 4.15.0
BuildRequires:  pkgconfig(garcon-gtk3-1) >= 0.6.0
BuildRequires:  pkgconfig(libxfce4ui-2) >= 4.15.7
BuildRequires:  pkgconfig(libxfce4util-1.0) >= 4.15.6
BuildRequires:  pkgconfig(libsystemd)
BuildRequires:  mesa-libGL-devel
BuildRequires:  pam-devel

Requires:       libxfce4ui >= 4.15.7
Requires:       garcon >= 0.6.0
Requires:       xfconf >= 4.15.0
Requires:       libxfce4util >= 4.15.6
Requires:       xfdesktop 
Requires:       systemd
Requires:       gtk-update-icon-cache

%description
xfce4-screensaver is a screen saver and locker that aims to have
simple, sane, secure defaults and be well integrated with the desktop.

%prep
%setup -q

%build
%configure --disable-static \
	--enable-pam \
    --enable-locking \
	--with-x \
	--with-mit-ext \
	--with-xf86gamma-ext \
	--with-xscreensaverdir=%{_datadir}/xscreensaver/config \
	--with-xscreensaverhackdir=%{_libexecdir}/xscreensaver \
	--with-libgl \
	--without-console-kit \
    --without-elogind \
	--with-systemd

%make_build

%install
%make_install

%find_lang %{name}

%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
if [ $1 -eq 0 ]; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null ||
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog INSTALL NEWS README.md TODO
%license COPYING COPYING.LGPL COPYING.LIB
%{_bindir}/*
%{_sysconfdir}/pam.d/%{name}
%{_sysconfdir}/xdg/autostart/%{name}.desktop
%{_sysconfdir}/xdg/menus/xfce4-screensavers.menu
%{_libexecdir}/%{name}/floaters
%{_libexecdir}/%{name}/popsquares
%{_libexecdir}/%{name}/slideshow
%{_libexecdir}/%{name}-dialog
%{_libexecdir}/%{name}-gl-helper
%{_datadir}/applications/screensavers/xfce-floaters.desktop
%{_datadir}/applications/screensavers/xfce-personal-slideshow.desktop
%{_datadir}/applications/screensavers/xfce-popsquares.desktop
%{_datadir}/applications/%{name}-preferences.desktop
%{_datadir}/dbus-1/services/org.xfce.ScreenSaver.service
%{_datadir}/desktop-directories/%{name}.directory
%{_datadir}/icons/hicolor/*/*/org.xfce.ScreenSaver.*
%{_datadir}/pixmaps/xfce-logo-white.svg
%{_mandir}/man1/*

%changelog
* Sun Jan 03 2021 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.16.0

* Sun Nov 15 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.1.11

* Sun Mar 29 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.1.10

* Tue Mar 24 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.1.9

* Mon Aug 12 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.1.8

* Sat Aug 10 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.1.7

* Sun Jun 30 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.1.6

* Thu Jun 13 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.1.5

* Fri Mar 22 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.1.4

* Fri Nov 23 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.1.3

* Sat Nov 03 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Generate additional savers from xscreensaver-base package
- Add patch for French locale

* Fri Nov 02 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.1.2

* Thu Nov 01 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.1.1

* Sat Oct 20 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Initial release (0.1.0)
