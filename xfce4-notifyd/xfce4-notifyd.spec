%global major 0.6

Name:           xfce4-notifyd
Version:        0.6.3
Release:        99%{?dist}
Summary:        Simple notification daemon for Xfce

License:        GPLv2
URL:            https://goodies.xfce.org/projects/applications/xfce4-notifyd
Source0:        https://archive.xfce.org/src/apps/%{name}/%{major}/%{name}-%{version}.tar.bz2

BuildRequires:  intltool
BuildRequires:  gettext
BuildRequires:  libtool

BuildRequires:  pkgconfig(x11)
BuildRequires:  pkgconfig(libxfce4util-1.0)
BuildRequires:  pkgconfig(libxfce4ui-2) >= 4.14.0
BuildRequires:  pkgconfig(libxfce4panel-2.0) >= 4.14.0
BuildRequires:  pkgconfig(libxfconf-0) >= 4.14.0
BuildRequires:  pkgconfig(gio-2.0)
BuildRequires:  pkgconfig(gio-unix-2.0)
BuildRequires:  pkgconfig(libnotify)
BuildRequires:  pkgconfig(gtk+-3.0) >= 3.20

Requires:       libxfce4ui >= 4.14.0
Requires:       xfce4-panel >= 4.14.0
Requires:       xfconf >= 4.14.0

BuildRequires:  desktop-file-utils
BuildRequires:  gtk-update-icon-cache

Requires:       hicolor-icon-theme

# for compatibility this package provides
Provides:       desktop-notification-daemon
# and obsoletes all notification-daemon-xfce releases
Obsoletes:      notification-daemon-xfce <= 0.3.7


%description
Xfce4-notifyd is a simple, visually-appealing notification daemon for Xfce 
that implements the freedesktop.org desktop notifications specification.
Features:
* Themable using the GTK+ theming mechanism
* Visually appealing: rounded corners, shaped windows
* Supports transparency and fade effects

%prep
%setup -q

%build
%configure --disable-static

%make_build

%install
%make_install

find %{buildroot} -name '*.la' | xargs %{__rm}

%find_lang %{name}

%check
desktop-file-validate %{buildroot}%{_datadir}/applications/%{name}-config.desktop

%ldconfig_scriptlets

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog NEWS README TODO
%license COPYING
%dir %{_sysconfdir}/xdg/autostart
%{_sysconfdir}/xdg/autostart/xfce4-notifyd.desktop
%{_bindir}/xfce4-notifyd-config
%{_libdir}/xfce4/notifyd/xfce4-notifyd
%{_libdir}/xfce4/panel/plugins/libnotification-plugin.so
%{_datadir}/applications/xfce4-notifyd-config.desktop
%{_datadir}/icons/hicolor/*/*/*
%dir %{_datadir}/themes
%dir %{_datadir}/themes/Default
%dir %{_datadir}/themes/Default/xfce-notify-4.0
%{_datadir}/themes/Default/xfce-notify-4.0/gtk.css
%dir %{_datadir}/themes/Smoke
%dir %{_datadir}/themes/Smoke/xfce-notify-4.0
%{_datadir}/themes/Smoke/xfce-notify-4.0/gtk.css
%dir %{_datadir}/themes/ZOMG-PONIES!
%dir %{_datadir}/themes/ZOMG-PONIES!/xfce-notify-4.0
%{_datadir}/themes/ZOMG-PONIES!/xfce-notify-4.0/gtk.css
%dir %{_datadir}/themes/Bright
%dir %{_datadir}/themes/Bright/xfce-notify-4.0
%{_datadir}/themes/Bright/xfce-notify-4.0/gtk.css
%dir %{_datadir}/themes/Retro
%dir %{_datadir}/themes/Retro/xfce-notify-4.0
%{_datadir}/themes/Retro/xfce-notify-4.0/gtk.css
%{_datadir}/xfce4/panel/plugins/notification-plugin.desktop
%{_mandir}/man1/xfce4-notifyd-config.1.gz

%changelog
* Sat Feb 12 2022 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.6.3

* Wed Sep 02 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.6.2

* Tue May 05 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.6.1

* Wed Apr 08 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.6.0

* Sun Oct 20 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Fix build with xfce4-panel >= 4.15

* Sat Jun 22 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Rebuild for Fedora 30

* Tue Apr 23 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.4.4

* Sat Oct 27 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.4.3

* Thu Mar 01 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.4.2

* Wed Dec 13 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.4.1

* Wed Oct 18 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.4.0

* Sat Aug 19 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Rebuilt

* Thu Aug 03 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.3.6-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.3.6-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Mon Mar 20 2017 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.3.6-1
- Update to 0.3.6

* Mon Feb 13 2017 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.3.5-1
- Update to 0.3.5

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.3.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Wed Nov 09 2016 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.3.4-1
- Update to 0.3.4

* Sun Oct 02 2016 Kevin Fenzi <kevin@scrye.com> - 0.3.3-1
- Update to 0.3.3. bugfix release

* Sat Sep 10 2016 Kevin Fenzi <kevin@scrye.com> - 0.3.2-1
- Update to 0.3.2. bugfix release

* Tue Sep 06 2016 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.3.1-1
- Update to 0.3.1. bugfix release

* Fri Jul 29 2016 Kevin Fenzi <kevin@scrye.com> - 0.3.0-1
- Update to 0.3.0. Fixes bug #1361562

* Fri Feb 05 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.2.4-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Fri Jun 19 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.4-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Sat Feb 28 2015 Kevin Fenzi <kevin@scrye.com> 0.2.4-7
- Rebuild again with provider and against Xfce 4.12

* Sat Feb 28 2015 Kevin Fenzi <kevin@scrye.com> 0.2.4-6
- Rebuild without provider to bootstrap against Xfce 4.12

* Sat Feb 21 2015 Till Maas <opensource@till.name> - 0.2.4-5
- Rebuilt for Fedora 23 Change
  https://fedoraproject.org/wiki/Changes/Harden_all_packages_with_position-independent_code

* Mon Aug 18 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.4-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Sun Jun 08 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.4-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Sun Aug 04 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Thu May 09 2013 Christoph Wickert <cwickert@fedoraproject.org> - 0.2.4-1
- Update to 0.2.4

* Mon Apr 22 2013 Christoph Wickert <cwickert@fedoraproject.org> - 0.2.3-1
- Update to 0.2.3 (fixes #759053 and #926785)
- BR libnotify-devel
- Drop upstreamed patches

* Fri Feb 15 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.2-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Sat Oct 06 2012 Christoph Wickert <cwickert@fedoraproject.org> - 0.2.2-6
- Remove obsolete libsexy checks
- Add patch to avoid flickering
- Add patch to support image URI locations
- Make xfce4-notifyd-config show up in xfce4-settings-manager

* Sun Jul 22 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.2-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Sun Apr 01 2012 Kevin Fenzi <kevin@scrye.com> - 0.2.2-4
- Rebuild for new lbxfce4util

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.2-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Tue Dec 06 2011 Adam Jackson <ajax@redhat.com> - 0.2.2-2
- Rebuild for new libpng

* Tue Aug 09 2011 Christoph Wickert <cwickert@fedoraproject.org> - 0.2.2-1
- Update to 0.2.2
- Remove upstreamed Fix-race-with-window-becoming-invalid.patch

* Sun Jul 31 2011 Christoph Wickert <cwickert@fedoraproject.org> - 0.2.1-3
- Fix crash in handle_error. Thanks to Ricky Zhou (#706677)
- Remove obsolete BuildRequires libglade2-devel

* Mon Feb 07 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Wed Feb 02 2011 Christoph Wickert <cwickert@fedoraproject.org> - 0.2.1-1
- Update to 0.2.1 (fixes #660549)
- Own %%{_libdir}/xfce4/notifyd/

* Sat Nov 27 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.2.0-1
- Update to 0.2.0

* Mon Jul 27 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Mon Feb 23 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.0-2
- Patch to rename dbus-service file to avoid conflict with notification-daemon
- Add Debian's patch support the reason arg in libnotify 0.4.5

* Mon Feb 23 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.0-1
- Initial Fedora Package
