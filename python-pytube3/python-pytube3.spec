%global srcname pytube3

Name:       python-%{srcname}
Version:	9.6.4
Release:	99%{?dist}
Summary:	Pythonic library for downloading YouTube videos

License:	MIT
URL:        https://github.com/get-pytube/pytube3
Source0:	https://files.pythonhosted.org/packages/source/p/%{srcname}/%{srcname}-%{version}.tar.gz

BuildArch:	noarch

%description
It is a lightweight, dependency-free Python3 library (and command-line
utility) for downloading YouTube videos.

%package -n python3-%{srcname}
Summary:	Python3 library for downloading YouTube videos

%description -n python3-%{srcname}
It is a lightweight, dependency-free Python3 library (and command-line
utility) for downloading YouTube videos.

BuildRequires:	python3-devel
BuildRequires:	python3-setuptools

%prep
%setup -q -n %{srcname}-%{version}

%build
%py3_build

%install
%py3_install

%files -n python3-%{srcname}
%license LICENSE
%doc README.md
%{python3_sitelib}/*
%{_bindir}/%{srcname}

%changelog
* Thu Mar 26 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 9.6.4
