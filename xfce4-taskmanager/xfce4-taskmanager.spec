%global major 1.5

Name:           xfce4-taskmanager
Version:        1.5.2
Release:        99%{?dist}
Summary:        Taskmanager for the Xfce desktop environment

License:        GPLv2+
URL:            https://docs.xfce.org/apps/xfce4-taskmanager
Source0:        https://archive.xfce.org/src/apps/%{name}/%{major}/%{name}-%{version}.tar.bz2

BuildRequires:	intltool
BuildRequires:	gettext
BuildRequires:	libtool

BuildRequires:	pkgconfig(cairo)
BuildRequires:	pkgconfig(xmu)
BuildRequires:	pkgconfig(x11)
BuildRequires:	pkgconfig(gtk+-3.0)
BuildRequires:	pkgconfig(libwnck-3.0)
BuildRequires:	pkgconfig(libxfce4ui-2) >= 4.15.0
BuildRequires:	pkgconfig(libxfconf-0) >= 4.15.0

BuildRequires:  gtk-update-icon-cache

%description
A simple taskmanager for the Xfce desktop environment.

%prep
%setup -q

%build
%configure \
    --without-skel
%make_build

%install
%make_install

%find_lang %{name}

%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
	touch --no-create %{_datadir}/icons/hicolor &>/dev/null
	gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files -f %{name}.lang
%doc AUTHORS ChangeLog NEWS THANKS
%license COPYING
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/*/*

%changelog
* Sat Feb 27 2021 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.5.2

* Sat Feb 13 2021 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.5.0

* Tue Dec 29 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.4.0

* Sat Apr 11 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.2.3

* Sat Jun 22 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Rebuild for Fedora 30

* Wed Dec 19 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.2.2

* Sun Jun 10 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.2.1

* Sat Aug 05 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Fix missing icons
- Add gtk3 option

* Thu Aug 03 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.2.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.2.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Sun Feb 12 2017 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 1.2.0-2
- Add icon files
- Fix buildrequires

* Sun Feb 12 2017 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 1.2.0-1
-Update to 1.2.0

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.1.0-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Fri Feb 05 2016 Fedora Release Engineering <releng@fedoraproject.org> - 1.1.0-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Fri Jun 19 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.1.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Sun May 03 2015 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 1.1.0-3
- Removed libxfcegui4 build requires
- Fixes 1209549

* Sat Feb 21 2015 Till Maas <opensource@till.name> - 1.1.0-2
- Rebuilt for Fedora 23 Change
  https://fedoraproject.org/wiki/Changes/Harden_all_packages_with_position-independent_code

* Fri Dec 26 2014 Kevin Fenzi <kevin@scrye.com> 1.1.0-1
- Update to 1.1.0

* Mon Aug 18 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Sun Jun 08 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Thu Jan 09 2014 Christoph Wickert <cwickert@fedoraproject.org> - 1.0.1-1
- Update to 1.0.1
- Remove aarch64 patch, no longer needed
- Add patch to fix some German translations (submitted upstream via Transifex)

* Sun Aug 04 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.0-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Sun May 19 2013 Christoph Wickert <cwickert@fedoraproject.org> - 1.0.0-7
- Make desktop vendor conditional
- Add aarch64 support (#926791)

* Sun Feb 10 2013 Parag Nemade <paragn AT fedoraproject DOT org> - 1.0.0-6
- Remove vendor tag from desktop file as per https://fedorahosted.org/fesco/ticket/1077
- Cleanup spec as per recently changed packaging guidelines

* Sun Jul 22 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.0-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Tue Dec 06 2011 Adam Jackson <ajax@redhat.com> - 1.0.0-3
- Rebuild for new libpng

* Mon Feb 07 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Tue Jun 15 2010 Christoph Wickert <cwickert@fedoraproject.org> - 1.0.0-1
- Update to 1.0.0 final

* Sun Jun 06 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.5.92-1
- Update to 0.5.91 (0.6 Beta 3)

* Tue May 25 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.5.91-1
- Update to 0.5.91 (0.6 Beta 2)

* Thu Jul 30 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.4.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Sat Feb 21 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.4.1-2
- Rebuild for i586 and SHA 256 hashes in RPM
- Rework categories of desktop file to allow nested menus

* Tue Sep 30 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.4.1-1
- Update to 0.4.1
- Remove patches (fixed upstream)

* Sat May 24 2008 Christoph Wickert <cwickert@fedoraproject.org> - 0.4.0-1
- Update to 0.4.0 stable which has finally been released
- Add patch to fix 0%-CPU bug (rebased version of Enrico Tröger's patch)
- Add patch to fix some compiler warnings (also based on Enrico's work)
- Update license tag

* Mon Feb 18 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.4.0-0.3.rc2
- Autorebuild for GCC 4.3

* Thu Oct 05 2006 Christoph Wickert <cwickert@fedoraproject.org> - 0.4.0-0.2.rc2
- Bump release for devel checkin.

* Tue Oct 03 2006 Christoph Wickert <cwickert@fedoraproject.org> - 0.4.0-0.1.rc2
- Update to 0.4-rc2.
- Add BR on perl(XML::Parser), drop intltool.

* Mon Oct 02 2006 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.1-6
- Rebuild for XFCE 4.4.

* Mon Sep 04 2006 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.1-5
- Mass rebuild for Fedora Core 6.
- Fix %%defattr.

* Sat Jun 03 2006 Christoph Wickert <fedora wickert at arcor de> - 0.3.1-4
- Add intltool BR (#193444).

* Thu Feb 16 2006 Christoph Wickert <fedora wickert at arcor de> - 0.3.1-3
- Rebuild for Fedora Extras 5.

* Thu Dec 15 2005 Christoph Wickert <fedora wickert at arcor de> - 0.3.1-2
- Initial Fedora Extras version.
- Add xfce4-taskmanager.desktop.
- Remove useless NEWS from %%doc.
- Clean up specfile.

* Sat Jul 09 2005 Christoph Wickert <fedora wickert at arcor de> - 0.3.1-1.fc4.cw
- Updated to version 0.3.1.
- Rebuild for Core 4.

* Thu Apr 14 2005 Christoph Wickert <fedora wickert at arcor de> - 0.2.1-1.fc3.cw
- Initial RPM release.
