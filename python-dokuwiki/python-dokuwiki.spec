%global srcname dokuwiki

Name:           python-%{srcname}
Version:        1.2.1
Release:        99%{?dist}
Summary:        Python module aims to manage DokuWiki via XML-RPC API

License:        MIT
URL:            https://github.com/fmenabe/python-dokuwiki
Source0:        https://files.pythonhosted.org/packages/source/d/%{srcname}/%{srcname}-%{version}.tar.gz

BuildArch:      noarch

%description
Python module aims to manage DokuWiki by using the provided XML-RPC API.

%package -n python3-%{srcname}
Summary:        Python3 module aims to manage DokuWiki via XML-RPC API

%description -n python3-%{srcname}
Python3 module aims to manage DokuWiki by using the provided XML-RPC API.

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools

%prep
%setup -q -n %{srcname}-%{version}

%build
%py3_build

%install
%py3_install

%files -n python3-%{srcname}
%license LICENSE
%{python3_sitelib}/*

%changelog
* Sat Feb 06 2021 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.2.1
