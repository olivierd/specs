%global srcname pytube

Name:       python-%{srcname}
Version:	9.5.1
Release:	1%{?dist}
Summary:	Pythonic library for downloading YouTube videos

License:	MIT
URL:        https://github.com/nficano/pytube
Source0:	https://files.pythonhosted.org/packages/source/p/%{srcname}/%{srcname}-%{version}.tar.gz

BuildArch:	noarch

%description
It is a lightweight, dependency-free Python library (and command-line
utility) for downloading YouTube videos.

%package -n python2-%{srcname}
Summary:	Python2 library for downloading YouTube videos
%{?python_provide:%python_provide python2-%{srcname}}

%description -n python2-%{srcname}
It is a lightweight, dependency-free Python2 library for downloading
YouTube videos.

BuildRequires:	python2-devel
BuildRequires:	python2-setuptools

%package -n python3-%{srcname}
Summary:	Python3 library for downloading YouTube videos

%description -n python3-%{srcname}
It is a lightweight, dependency-free Python3 library (and command-line
utility) for downloading YouTube videos.

BuildRequires:	python3-devel
BuildRequires:	python3-setuptools

%prep
%setup -q -n %{srcname}-%{version}

%build
%py2_build
%py3_build

%install
%py2_install
%py3_install

%files -n python2-%{srcname}
%license LICENSE
%doc README.md
%{python2_sitelib}/*

%files -n python3-%{srcname}
%license LICENSE
%doc README.md
%{python3_sitelib}/*
%{_bindir}/%{srcname}

%changelog
* Sat Jun 29 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 9.5.1

* Sat May 04 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 9.5.0

* Tue Jan 29 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 9.4.0

* Wed Dec 26 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Add patch

* Fri Nov 09 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 9.3.5

* Fri Oct 19 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 9.3.3

* Sat Oct 13 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 9.3.0

* Sat Sep 15 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 9.2.3

* Thu Apr 12 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 9.2.2

* Thu Mar 15 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 9.1.0

* Sat Feb 10 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 9.0.6

* Fri Jan 12 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 8.0.2

* Fri Jan 05 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 8.0.0

* Sun Nov 12 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 7.0.18
