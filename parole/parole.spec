%global major 4.16

# https://git.xfce.org/apps/parole/tree/NEWS#n42
%if 0%{?fedora} >= 26
%global pkgconf pkgconf
%else
%global pkgconf pkg-config
%endif
%global gtk3_version %(%{pkgconf} --modversion gtk+-3.0 2>/dev/null | %{__awk} -F '.' '{printf("%s%s", $1, $2);}')

Name:           parole
Version:        4.16.0
Release:        99%{?dist}
Summary:        Media player for the Xfce desktop

License:        GPLv2+
URL:            https://docs.xfce.org/apps/parole/start
Source0:        https://archive.xfce.org/src/apps/%{name}/%{major}/%{name}-%{version}.tar.bz2

BuildRequires:	libtool
BuildRequires:	intltool
BuildRequires:	gettext

BuildRequires:	pkgconfig(glib-2.0)
BuildRequires:	pkgconfig(gobject-2.0)
BuildRequires:	pkgconfig(gthread-2.0)
BuildRequires:	pkgconfig(gmodule-2.0)
BuildRequires:	pkgconfig(gio-2.0)
BuildRequires:	pkgconfig(dbus-1)
BuildRequires:	pkgconfig(dbus-glib-1)
BuildRequires:	pkgconfig(libxfce4util-1.0)
BuildRequires:	pkgconfig(libxfconf-0)
BuildRequires:	pkgconfig(libxfce4ui-2) >= 4.15.0
BuildRequires:	pkgconfig(gtk+-3.0)
BuildRequires:	pkgconfig(gstreamer-1.0)
BuildRequires:	pkgconfig(gstreamer-base-1.0)
BuildRequires:	pkgconfig(gstreamer-video-1.0)
BuildRequires:	pkgconfig(gstreamer-pbutils-1.0)
BuildRequires:	pkgconfig(x11)
%if 0%{?gtk3_version} &&  0%{?gtk3_version} < 322
BuildRequires:	pkgconfig(clutter-1.0)
BuildRequires:	pkgconfig(clutter-gtk-1.0)
%endif
BuildRequires:	pkgconfig(taglib)
BuildRequires:	pkgconfig(libnotify)

BuildRequires:  shared-mime-info
BuildRequires:  gtk-update-icon-cache
BuildRequires:  desktop-file-utils
BuildRequires:  libappstream-glib

Requires:	gstreamer1-plugins-good
Requires:	xdg-utils
Requires:	libxfce4ui >= 4.15.0

%description
Parole is a modern simple media player based on the GStreamer framework and 
written to fit well in the Xfce desktop. Parole features playback of local 
media files, DVD/CD and live streams. Parole is extensible via plugins.

The project still in its early developments stage, but already contains the 
following features:
* Audio playback
* Video playback with optional subtitle
* Playback of live sources

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains header files for developing plugins for 
%{name}.

%prep
%setup -q

%{__sed} -i 's;video/x-totem-stream;#video/x-totem-stream;' data/mime/mime-type-list.txt

%build
%configure --disable-static \
	 --enable-mpris2-plugin \
	 --without-html-dir \
%if 0%{?gtk3_version} &&  0%{?gtk3_version} < 322
	 --enable-clutter
%else
	 --disable-clutter
%endif
%make_build

%install
%make_install

find %{buildroot} -name "*.la" | xargs %{__rm}

%find_lang %{name}

# Failed to parse ...
##%check
##appstream-util validate-relax --nonet \
##    %{buildroot}%{_datadir}/metainfo/parole.appdata.xml


%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
update-mime-database %{_datadir}/mime &> /dev/null || :

%postun
if [ $1 -eq 0 ] ; then
  touch --no-create %{_datadir}/icons/hicolor &>/dev/null
  gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi
update-mime-database %{_datadir}/mime &> /dev/null || :

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS TODO THANKS
%license COPYING
%{_bindir}/%{name}
%{_libdir}/parole-0/*.so
%{_datadir}/applications/org.xfce.Parole.desktop
%{_datadir}/icons/hicolor/*/*/*
%{_datadir}/parole/parole-plugins-0/*.desktop
%{_datadir}/parole/pixmaps/*.png
%{_metainfodir}/parole.appdata.xml

%files devel
%defattr(-,root,root,-)
%{_includedir}/%{name}/*

%changelog
* Mon Jan 11 2021 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.16.0

* Sun Dec 27 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.15.0

* Fri Nov 15 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.0.5

* Sun Aug 11 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.0.4

* Sat Jul 27 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.0.3

* Thu Apr 04 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.0.2

* Wed Nov 07 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Sync with Fedora 29

* Thu Apr 12 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.0.1

* Thu Mar 01 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.0.0

* Sun Jul 30 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Add new patch for missing icon in parole-settings.ui

* Sat Jun 10 2017 Mike DePaulo <mikedep333@gmail.com> - 0.9.2-1
- Upgrade to 0.9.2
- Patch parole.appdata.xml so that it validates
- Clean up parole-0.8.0-appdata.patch
- Run autogen.sh to regenerate ./configure (because Fedora suggests to do so)

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.8.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.8.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Thu Oct 15 2015 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.8.1-1
- Update to 0.8.1
- Remove appdata patch (added by upstream)

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.8.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Sat Mar 07 2015 Kevin Fenzi <kevin@scrye.com> 0.8.0-2
- Switch to gstreamer1 and build clutter backend
- Clean up BuildRequires versions
- Properly validate and install appdata

* Sat Feb 28 2015 Kevin Fenzi <kevin@scrye.com> 0.8.0-1
- Upgrade to 0.8.0
- Rebuild for Xfce 4.12

* Sat Feb 21 2015 Till Maas <opensource@till.name> - 0.5.4-7
- Rebuilt for Fedora 23 Change
  https://fedoraproject.org/wiki/Changes/Harden_all_packages_with_position-independent_code

* Sun Feb 01 2015 Richard Hughes <richard@hughsie.com> 0.5.4-6
- Fix the AppData file; you can only use <_p> in a file that gets passed to
  intltool, i.e. translated upstream. Downstream files have to be valid.

* Tue Nov 11 2014 Kevin Fenzi <kevin@scrye.com> 0.5.4-5
- Add appdata file. Fixes #1162380

* Sun Aug 17 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5.4-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Fri Jun 06 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5.4-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Sat Jan 04 2014 Christoph Wickert <cwickert@fedoraproject.org> - 0.5.4-2
- Add upstream patch to correctly close dir-handle (#963428)

* Fri Dec 20 2013 Kevin Fenzi <kevin@scrye.com> 0.5.4-1
- Update to 0.5.4 (fixes #1045255)

* Fri Jul 26 2013 Christoph Wickert <cwickert@fedoraproject.org> - 0.5.2-1
- Update to 0.5.2 (fixes #972567)

* Fri Jun 07 2013 Christoph Wickert <cwickert@fedoraproject.org> - 0.5.1-1
- Update to 0.5.1 (#971293)

* Wed Mar 06 2013 Kevin Fenzi <kevin@scrye.com> 0.5.0-1
- Update to 0.5.0

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.4.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Mon Jan 07 2013 Christoph Wickert <cwickert@fedoraproject.org> - 0.4.0-1
- Update to 0.4.0
- BR xfconf-devel as configuration is now stored in xfconf

* Wed Aug 22 2012 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.0.3-1
- Update to 0.3.0.3
- Build gtk documentation again (bugzilla.xfce.org #9232)

* Tue Aug 21 2012 Christoph Wickert <cwickert@fedoraproject.org> - 0.3.0.2-1
- Update to 0.3.0.2
- For now, don't build with --enable-gtk-doc
- Drop DSO patch, fixed upstream

* Fri Jul 20 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.0.6-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Thu Apr 05 2012 Kevin Fenzi <kevin@scrye.com> - 0.2.0.6-4
- Rebuild for Xfce 4.10

* Fri Jan 13 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.0.6-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Tue Dec 06 2011 Adam Jackson <ajax@redhat.com> - 0.2.0.6-2
- Rebuild for new libpng

* Sun Jun 05 2011 Christoph Wickert <cwickert@fedoraproject.org> - 0.2.0.6-1
- Update to 0.2.0.6
- Obsolete parole-mozplugin
- Drop libnotify patch, no longer needed

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.0.2-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Fri Nov 05 2010 Kevin Fenzi <kevin@tummy.com> - 0.2.0.2-5
- Add patch to build against new libnotify 

* Wed Sep 29 2010 jkeating - 0.2.0.2-4
- Rebuilt for gcc bug 634757

* Sat Sep 18 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.2.0.2-3
- Remove mime-type "video/x-totem-stream" (#633304)
- Make parole-devel no longer require gtk-doc (#604409)

* Thu Feb 18 2010 Kevin Fenzi <kevin@tummy.com> - 0.2.0.2-2
- Add patch to fix DSO issue. Fixes bug #565207

* Mon Jan 25 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.2.0.2-1
- Update to 0.2.0.2

* Thu Jan 14 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.2.0.1-1
- Update to 0.2.0.1

* Tue Jan 05 2010 Christoph Wickert <cwickert@fedoraproject.org> - 0.2.0-1
- Update to 0.2.0

* Tue Dec 01 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.99-1
- Update to 0.1.99 and drop all patches
- Make the plugin require mozilla-filesystem

* Sun Nov 29 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.98-1
- Update to 0.1.98
- Cherry pick some patches to build the browser plugin with xulrunner 1.9.2

* Wed Nov 25 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.96-1
- Update to 0.1.96
- Build gtk-doc files

* Wed Nov 25 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.95-1
- Update to 0.1.95

* Fri Oct 30 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.91-1
- Update to 0.1.91

* Thu Oct 08 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.90-3
- Set locale before loading the GtkBuilder interface definition for dialogs
- Translation updates

* Thu Oct 08 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.90-2
- BuildRequire taglib-devel and fix libnotify requirement

* Wed Oct 07 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.1.90-1
- Update to 0.1.90
- Loads of additional translations

* Fri Sep 18 2009 Christoph Wickert <cwickert@fedoraproject.org> - 0.1-0.1
- Initial package
