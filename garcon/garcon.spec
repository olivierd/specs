%global major 4.17

Name:	garcon
Version:	4.17.0
Release:	99%{?dist}
Summary:	Implementation of the freedesktop.org menu specification

# garcon's source code is licensed under the LGPLv2+,
# while its documentation is licensed under the GFDL 1.1
License:	LGPLv2+ and GFDL
Source0:	https://archive.xfce.org/src/xfce/%{name}/%{major}/%{name}-%{version}.tar.bz2

BuildRequires:	intltool
BuildRequires:	gettext
BuildRequires:	libtool

BuildRequires:	pkgconfig(glib-2.0)
BuildRequires:	pkgconfig(gio-2.0)
BuildRequires:	pkgconfig(gobject-2.0)
BuildRequires:	pkgconfig(gthread-2.0)
BuildRequires:	pkgconfig(gtk+-3.0)
BuildRequires:	pkgconfig(libxfce4util-1.0)
BuildRequires:	pkgconfig(libxfce4ui-2)
BuildRequires:	pkgconfig(gobject-introspection-1.0)
BuildRequires:	gtk-update-icon-cache

%description
Garcon is an implementation of the freedesktop.org menu specification
replacing the former Xfce menu library libxfce4menu. It is based on
GLib/GIO only and aims at covering the entire specification except
for legacy menus.

%package	devel
Summary:	Development files for %{name}
Requires:	%{name}%{?_isa} = %{version}-%{release}
Requires:	libxfce4ui-devel
Requires:	glib2-devel
Requires:	gtk3-devel
Requires:	gobject-introspection-devel

%description	devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q

%build
%configure --disable-static \
	--enable-introspection=yes \
	--without-html-dir

# Remove rpaths
%{__sed} -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
%{__sed} -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

export LD_LIBRARY_PATH=$(pwd)/garcon-gtk/.libs:${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
export LD_LIBRARY_PATH=$(pwd)/garcon/.libs:${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}

%make_build

%install
%make_install

find %{buildroot} -name '*.la' | xargs %{__rm}

%find_lang %{name}

%ldconfig_scriptlets devel

%post
update-desktop-database &> /dev/null || :
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
update-desktop-database &> /dev/null ||:
if [ $1 -eq 0 ]; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files -f %{name}.lang
%defattr(-,root,root,-)
%license COPYING
%doc AUTHORS NEWS README.md
%{_sysconfdir}/xdg/menus/xfce-applications.menu
%{_libdir}/libgarcon-1.so.*
%{_libdir}/libgarcon-gtk3-1.so.*
%{_datadir}/desktop-directories/*.directory
%{_datadir}/icons/hicolor/32x32/apps/org.xfce.garcon.png

%files	devel
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/libgarcon-1.so
%{_libdir}/libgarcon-gtk3-1.so
%{_libdir}/girepository-1.0/Garcon-1.0.typelib
%{_libdir}/girepository-1.0/GarconGtk-1.0.typelib
%{_libdir}/pkgconfig/garcon-1.pc
%{_libdir}/pkgconfig/garcon-gtk3-1.pc
%{_datadir}/gir-1.0/Garcon-1.0.gir
%{_datadir}/gir-1.0/GarconGtk-1.0.gir

%changelog
* Mon May 16 2022 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.17.0

* Fri Jan 15 2021 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 4.16.1

* Sat Dec 26 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.8.0

* Mon Dec 14 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.7.3

* Mon Nov 09 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.7.2

* Wed Aug 19 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.7.1

* Sat May 30 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Rename exo-helpers (exo >= 4.15.1)

* Sat Apr 04 2020 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.7.0

* Sun Dec 22 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Remove support of Gtk2 (consequence of update of libxfce4ui >= 4.15.0)

* Sat Jul 27 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.6.4

* Mon Jul 01 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.6.3

* Fri Feb 15 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Add support for GObject introspection

* Fri Dec 07 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.6.2

* Tue Nov 06 2018 Olivier Duchateau <duchateau.olivier@gmail.com>
- Sync with Fedora 29

* Mon Aug 21 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Apply patch from bug #13785

* Sun Jul 09 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Rebuild for update of libxfce4ui

* Sun Jun 11 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.6.1

* Sun Apr 16 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 0.6.0
