%global desc \
%{name} is a ctypes-based Python interface to the mpv media \
player. It gives you more or less full control of all features \
of the player.


Name:          python-mpv
Version:       0.3.9
Release:       1%{?dist}
Summary:       Python interface to the MPV media player

License:       AGPLv3
URL:           https://github.com/jaseg/python-mpv
Source0:       https://files.pythonhosted.org/packages/source/p/%{name}/%{name}-%{version}.tar.gz

BuildRequires: mpv-libs-devel
BuildArch:     noarch

%description %desc

%package -n python%{python3_pkgversion}-mpv
Summary:       %{summary}
%{?python_provide:%python_provide python%{python3_pkgversion}-mpv}
BuildRequires: python%{python3_pkgversion}-setuptools
BuildRequires: python%{python3_pkgversion}-devel
BuildRequires: python%{python3_pkgversion}-pillow

%description -n python%{python3_pkgversion}-mpv %desc

%prep
%setup -q -n %{name}-%{version}

%build
%py3_build

%install
%py3_install

%files -n python%{python3_pkgversion}-mpv
%doc README.rst
%{python3_sitelib}/mpv.py
%{python3_sitelib}/__pycache__/mpv.*.pyc
%{python3_sitelib}/python_mpv-%{version}-*.egg-info/

%changelog
* Thu Nov 07 2019 Olivier Duchateau <duchateau.olivier@gmail.com>
- Initial import (0.3.9)
