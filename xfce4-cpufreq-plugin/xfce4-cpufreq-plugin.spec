%global major 1.1

Name:		xfce4-cpufreq-plugin
Version:	1.1.90
Release:	1%{?dist}
Summary:	Shows CPU frequencies and governor

License:	GPLv2+
URL:		https://goodies.xfce.org/projects/panel-plugins/%{name}
Source0:	http://archive.xfce.org/src/panel-plugins/%{name}/%{major}/%{name}-%{version}.tar.bz2
Patch0:	panel-plugin_xfce4-cpufreq-plugin.patch

BuildRequires:	libtool	
BuildRequires:	intltool
BuildRequires:	gettext

BuildRequires:	pkgconfig(gthread-2.0)
BuildRequires:	pkgconfig(gtk+-3.0)
BuildRequires:	pkgconfig(libxfce4ui-2) >= 4.13.0
BuildRequires:	pkgconfig(libxfce4panel-2.0) >= 4.13.0
BuildRequires:	gtk-update-icon-cache

Requires:	libxfce4ui%{?_isa} >= 4.13.0
Requires:	xfce4-panel%{?_isa} >= 4.13.0

%description
Panel plugin shows information about the CPU
governor and frequencies supported and used
by your system

%prep
%setup -q

%patch0

%build
%configure --disable-static
make %{?_smp_mflags}

%install
%make_install DESTDIR=%{buildroot} INSTALL="%{__install} -p"

find %{buildroot} -name '*.la' | xargs %{__rm}

%find_lang %{name}

%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
if [ $1 -eq 0 ]; then
	touch --no-create %{_datadir}/icons/hicolor &>/dev/null
	gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files -f %{name}.lang
%doc AUTHORS NEWS README TODO
%license COPYING
%{_datadir}/xfce4/panel/plugins/*.desktop
%{_datadir}/icons/hicolor/*/*/*
%{_libdir}/xfce4/panel/plugins/libcpufreq.so


%changelog
* Sun Nov 05 2017 Olivier Duchateau <duchateau.olivier@gmail.com>
- Update to 1.1.90

